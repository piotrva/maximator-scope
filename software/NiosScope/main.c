/* 
 * NiosScope
 */

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>

#include "sys/alt_irq.h"
#include "system.h"
#include "altera_avalon_pio_regs.h"
#include "altera_avalon_timer_regs.h"
#include "sys/alt_stdio.h"

#include "DisplayDriver.h"
#include "SequencerDriver.h"
#include "Functions.h"
#include "KBD.h"

#define CHA_ADC_NUM 6
#define CHB_ADC_NUM 7

volatile scopeConfig_t scopeConfig={
		.channelA={
				.gain = GAIN_1,
				.offset = 0,
				.enabled = 1
		},
		.channelB={
				.gain = GAIN_1,
				.offset = 0,
				.enabled = 1
		},
		.channelMath={
				.gain = GAIN_1,
				.offset = 0,
				.enabled = 1
		},
		.mathMode = MATH_AplusB,
		.triggerVal = 0,
		.triggerHyst = 10,
		.timeBase = TIMEBASE_05,
		.triggerEdge = TRIG_RISE,
		.triggerSource = TRIG_CHA,
		.triggerMode = TRIG_NORMAL,
		.triggerState = TRIG_READY,
		.activeMenu = 0,
		.triggerAuto = 500,
		.timeout = 500
};

volatile int counter = 0;

#define DEBOUNCE_TIME 40

// 1ms interrupt
static void irr(){
	if(scopeConfig.timeout){
		scopeConfig.timeout--;
	}else{
		if(scopeConfig.triggerMode == TRIG_AUTO && scopeConfig.triggerState != TRIG_STOP){
			scopeConfig.timeout = scopeConfig.triggerAuto;
			triggerForce(SCOPESEQUENCER_0_BASE);
		}
	}
	counter ++;
	IOWR_ALTERA_AVALON_PIO_DATA(LED_IO_BASE, counter/100);
	IOWR_ALTERA_AVALON_TIMER_STATUS(TIM0_BASE, 0);


	kbdCheck();
}

volatile bool isSamplingDone = false;

static void samplingDone(){
	resetIRQ(SCOPESEQUENCER_0_BASE); //reset IRQ
	isSamplingDone = true;
}

int main()
{ 
	menu_items[KBD_OK].click.action = menuClick;

	menu_items[KBD_UP].click.action = menuPlus;
	menu_items[KBD_UP].hold.action = menuPlus;

	menu_items[KBD_DN].click.action = menuMinus;
	menu_items[KBD_DN].hold.action = menuMinus;

	IOWR_ALTERA_AVALON_TIMER_CONTROL(TIM0_BASE, ALTERA_AVALON_TIMER_CONTROL_START_MSK |
		ALTERA_AVALON_TIMER_CONTROL_CONT_MSK | ALTERA_AVALON_TIMER_CONTROL_ITO_MSK );

	alt_ic_isr_register(TIM0_IRQ_INTERRUPT_CONTROLLER_ID,TIM0_IRQ, irr, NULL, 0);

	applySettings(&scopeConfig);
	displaySettings(&scopeConfig);

	setDisplayParam(SCOPEDISPLAY_0_BASE + SCOPEDISPLAY_CFG_TEXTCOLOR, SCOPEDISPLAY_COLOR_G);

	alt_ic_isr_register(SCOPESEQUENCER_0_IRQ_INTERRUPT_CONTROLLER_ID,SCOPESEQUENCER_0_IRQ, samplingDone, NULL, 0);

	triggerArm(SCOPESEQUENCER_0_BASE);  //arm trigger

	alt_putstr("NiosScope ready!\r\n");

	/* Event loop never exits. */
	while (1){
		/*char c = alt_getchar();
		alt_putchar(c);
		alt_putchar('\r');
		alt_putchar('\n');
		alt_irq_context context = alt_irq_disable_all();
		if(c=='q')menuMinus();
		if(c=='w')menuClick();
		if(c=='e')menuPlus();
		alt_irq_enable_all(context);*/

		alt_irq_context context = alt_irq_disable_all();

		if(isSamplingDone){
			isSamplingDone = false;
			alt_irq_enable_all(context);
			switch(scopeConfig.triggerMode){
			case TRIG_AUTO:

				break;
			case TRIG_NORMAL:

				break;
			case TRIG_SINGLE:

				break;
			}

			//if trigger stopped - cancel aquisition
			if(scopeConfig.triggerState == TRIG_STOP) continue;

			int32_t a = 0, b = 0, as=0, bs=0;
			if(scopeConfig.channelA.enabled && scopeConfig.channelB.enabled){
				for(int i = 0 ; i < 1024 ; i+=1){
					if(i%2 == 0){
						a = readData(SCOPESEQUENCER_0_BASE, i);
						as = applyGain(scopeConfig.channelA.gain, a) + scopeConfig.channelA.offset;
						setDisplayData(SCOPEDISPLAY_0_BASE + SCOPEDISPLAY_G0_BASE, i, as);
						setDisplayData(SCOPEDISPLAY_0_BASE + SCOPEDISPLAY_G0_BASE, i+1, as);
					}else{
						b = readData(SCOPESEQUENCER_0_BASE, i);
						bs = applyGain(scopeConfig.channelB.gain, b) + scopeConfig.channelB.offset;
						setDisplayData(SCOPEDISPLAY_0_BASE + SCOPEDISPLAY_G1_BASE, i-1, bs);
						setDisplayData(SCOPEDISPLAY_0_BASE + SCOPEDISPLAY_G1_BASE, i, bs);
						int32_t math = applyMath(scopeConfig.mathMode, a, b);
						math = applyGain(scopeConfig.channelMath.gain, math) + scopeConfig.channelMath.offset;
						setDisplayData(SCOPEDISPLAY_0_BASE + SCOPEDISPLAY_G2_BASE, i-1, math);
						setDisplayData(SCOPEDISPLAY_0_BASE + SCOPEDISPLAY_G2_BASE, i, math);
					}
				}
			}else if(scopeConfig.channelA.enabled){
				for(int i = 0 ; i < 1024 ; i+=1){
					a = readData(SCOPESEQUENCER_0_BASE, i);
					as = applyGain(scopeConfig.channelA.gain, a) + scopeConfig.channelA.offset;
					setDisplayData(SCOPEDISPLAY_0_BASE + SCOPEDISPLAY_G0_BASE, i, as);
				}
			}else{
				for(int i = 0 ; i < 1024 ; i+=1){
					a = readData(SCOPESEQUENCER_0_BASE, i);
					as = applyGain(scopeConfig.channelB.gain, a) + scopeConfig.channelB.offset;
					setDisplayData(SCOPEDISPLAY_0_BASE + SCOPEDISPLAY_G1_BASE, i, as);
				}
			}

			triggerUnForce(SCOPESEQUENCER_0_BASE);

			switch(scopeConfig.triggerMode){
			case TRIG_AUTO:
				triggerArm(SCOPESEQUENCER_0_BASE);  //arm trigger
				scopeConfig.timeout = scopeConfig.triggerAuto;
				scopeConfig.triggerState = TRIG_TRIGD;
				break;
			case TRIG_NORMAL:
				triggerArm(SCOPESEQUENCER_0_BASE);  //arm trigger
				scopeConfig.triggerState = TRIG_TRIGD;
				break;
			case TRIG_SINGLE:
				scopeConfig.triggerState = TRIG_TRIGD;
				break;
			}
			displaySettings(&scopeConfig);
		}
		alt_irq_enable_all(context);
		usleep(1000);

	}

	return 0;
}
