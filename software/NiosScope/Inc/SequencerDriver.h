#ifndef _SEQUENCERDRIVER_H
#define _SEQUENCERDRIVER_H

#include <stdint.h>
#include "system.h"
#include "io.h"

#include "ScopeSequencerRegs.h"

int32_t readData(uint32_t base, uint32_t offset);

void triggerArm(uint32_t base);
void triggerForce(uint32_t base);
void triggerUnForce(uint32_t base);
void resetIRQ(uint32_t base);

void setSequencerParam(uint32_t base, uint32_t param);

#endif //_SEQUENCERDRIVER_H
