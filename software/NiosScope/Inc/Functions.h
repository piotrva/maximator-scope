/*
 * Functions.h
 *
 *  Created on: 1 May 2017
 *      Author: Piotr
 */

#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

#include <stdint.h>
#include <stdio.h>
#include "DisplayDriver.h"
#include "SequencerDriver.h"
#include "system.h"
#include "io.h"

typedef enum {
	GAIN_01,
	GAIN_02,
	GAIN_03,
	GAIN_04,
	GAIN_05,
	GAIN_06,
	GAIN_07,
	GAIN_08,
	GAIN_09,
	GAIN_1,
	GAIN_2,
	GAIN_3,
	GAIN_4,
	GAIN_5,
	GAIN_6,
	GAIN_7,
	GAIN_8,
	GAIN_9,
	GAIN_10
} gain_t;

typedef enum {
	MATH_AplusB,
	MATH_AminusB,
	MATH_BminusA,
	MATH_AdivB,
	MATH_BdivA,
	MATH_AmultB
} math_t;

typedef struct{
	gain_t gain;
	int32_t offset;
	uint8_t enabled;
} channel_t;

typedef enum{
	TIMEBASE_01,
	TIMEBASE_02,
	TIMEBASE_05,
	TIMEBASE_1,
	TIMEBASE_2,
	TIMEBASE_5,
	TIMEBASE_10,
	TIMEBASE_20,
	TIMEBASE_50,
	TIMEBASE_100
} timebase_t;//390.625 -> 0.5ms/div 78.125 -> 0.1ms/div

typedef enum{
	TRIG_RISE,
	TRIG_FALL
} triggerEdge_t;

typedef enum{
	TRIG_CHA,
	TRIG_CHB
} triggerSource_t;

typedef enum{
	TRIG_NORMAL,
	TRIG_AUTO,
	TRIG_SINGLE
} triggerMode_t;

typedef enum{
	TRIG_READY,
	TRIG_TRIGD,
	TRIG_STOP
} triggerState_t;

typedef struct{
	channel_t channelA;
	channel_t channelB;
	channel_t channelMath;
	math_t mathMode;
	int32_t triggerVal;
	uint32_t triggerHyst;
	timebase_t timeBase;
	triggerEdge_t triggerEdge;
	triggerSource_t triggerSource;
	triggerMode_t triggerMode;
	triggerState_t triggerState;
	uint8_t activeMenu;
	uint32_t triggerAuto;
	uint32_t timeout;
} scopeConfig_t;

#define CHA_ADC_NUM 6
#define CHB_ADC_NUM 7

extern volatile scopeConfig_t scopeConfig;

void displaySettings(volatile scopeConfig_t* config);
void applySettings(volatile scopeConfig_t* config);

int32_t applyGain(gain_t gain, int32_t x);
int32_t applyMath(math_t mathMode, int32_t a, int32_t b);

void menuPlus(uint8_t param);
void menuMinus(uint8_t param);
void menuClick(uint8_t param);

#endif /* MENU_H_ */
