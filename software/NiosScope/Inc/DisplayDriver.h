#ifndef _DISPLAYDRIVER_H
#define _DISPLAYDRIVER_H

#include <stdint.h>
#include "system.h"
#include "io.h"

#include "Font.h"
#include "ScopeDisplayRegs.h"

void putSymbol(uint32_t symbol, uint32_t loc);
void putString(char string[], uint32_t loc);

void setDisplayData(uint32_t base, uint32_t offset, int32_t data);
void setDisplayParam(uint32_t base, uint32_t param);

void enableDisplay(uint32_t base, uint32_t func);
void disableDisplay(uint32_t base, uint32_t func);

#endif //_DISPLAYDRIVER_H
