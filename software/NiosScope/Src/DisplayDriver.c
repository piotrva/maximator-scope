#include "DisplayDriver.h"

const char charset[] = " *+-./0123456789:<>ABCDEFGHIMNORSTVmsu";

char decodeChar(char in){
	int i = 0;
	while(charset[i]){
		if(in == charset[i]) return (char)i;
		i++;
	}
	return 0;
}

void putString(char string[], uint32_t loc){
	uint32_t i = 0;
	while(string[i]){
		putSymbol(decodeChar(string[i]), loc + i);
		i++;
	}
}

void putSymbol(uint32_t symbol, uint32_t loc){
	for(int i = 0 ; i < FONT_WIDTH ; i++){
		IOWR_32DIRECT(SCOPEDISPLAY_0_BASE + SCOPEDISPLAY_TXT_BASE + 4*(i+loc*FONT_WIDTH),
				0, ~(((uint16_t)Font[FONT_WIDTH*2*symbol+i*2]) * 256 +
						((uint16_t)Font[FONT_WIDTH*2*symbol+i*2+1]))
		);
	}
}

void setDisplayData(uint32_t base, uint32_t offset, int32_t data){
	if(data > 256) data = 256;
	if(data < -255) data = -255;
	IOWR_32DIRECT(base + offset * 4, 0, data+255);
}

void setDisplayParam(uint32_t base, uint32_t param){
	IOWR_32DIRECT(base, 0, param);
}

void enableDisplay(uint32_t base, uint32_t func){
	uint32_t actual = IORD_32DIRECT(base + SCOPEDISPLAY_CFG_ENABLE, 0);
	actual |= func;
	IOWR_32DIRECT(base + SCOPEDISPLAY_CFG_ENABLE, 0, actual);
}

void disableDisplay(uint32_t base, uint32_t func){
	uint32_t actual = IORD_32DIRECT(base + SCOPEDISPLAY_CFG_ENABLE, 0);
	actual &= ~func;
	IOWR_32DIRECT(base + SCOPEDISPLAY_CFG_ENABLE, 0, actual);
}
