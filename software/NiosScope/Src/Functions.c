#include "Functions.h"

const char gainNames[19][4]={
		"*.1",
		"*.2",
		"*.3",
		"*.4",
		"*.5",
		"*.6",
		"*.7",
		"*.8",
		"*.9",
		"*1 ",
		"*2 ",
		"*3 ",
		"*4 ",
		"*5 ",
		"*6 ",
		"*7 ",
		"*8 ",
		"*9 ",
		"*10"
};

const char mathNames[6][4]={
	"A+B",
	"A-B",
	"B-A",
	"A/B",
	"B/A",
	"A*B"
};

const char timebaseNames[10][6]={
	"0.1ms",
	"0.2ms",
	"0.5ms",
	"  1ms",
	"  2ms",
	"  5ms",
	" 10ms",
	" 20ms",
	" 50ms",
	"100ms"
};

const char trigEdgeNames[2]={
		'R',
		'F'
};

const char trigSourceNames[2]={
		'A',
		'B'
};

const char trigModeNames[3]={
		'N',
		'A',
		'S'
};

const char trigStateNames[3]={
		'R',
		'T',
		'S'
};

const uint32_t timebasePrescallers[10]={
		78,
		156,
		391,
		781,
		1562,
		3906,
		7812,
		15625,
		39062,
		78125
};

const uint32_t triggerAutos[10]={
		200,
		200,
		500,
		500,
		500,
		500,
		1000,
		2000,
		5000,
		10000
};

int32_t applyGain(gain_t gain, int32_t x){
	switch(gain){
	case GAIN_01: return (x*1)/10;
	case GAIN_02: return (x*2)/10;
	case GAIN_03: return (x*3)/10;
	case GAIN_04: return (x*4)/10;
	case GAIN_05: return (x*5)/10;
	case GAIN_06: return (x*6)/10;
	case GAIN_07: return (x*7)/10;
	case GAIN_08: return (x*8)/10;
	case GAIN_09: return (x*9)/10;
	case GAIN_1: return x;
	case GAIN_2: return x*2;
	case GAIN_3: return x*3;
	case GAIN_4: return x*4;
	case GAIN_5: return x*5;
	case GAIN_6: return x*6;
	case GAIN_7: return x*7;
	case GAIN_8: return x*8;
	case GAIN_9: return x*9;
	case GAIN_10: return x*10;
	}
	return 0;
}

int32_t applyMath(math_t mathMode, int32_t a, int32_t b){
	int32_t math = 0;
	switch(mathMode){
	case MATH_AplusB:
		math = a + b;
		break;
	case MATH_AminusB:
		math = a - b;
		break;
	case MATH_BminusA:
		math = b - a;
		break;
	case MATH_AmultB:
		math = a * b;
		break;
	case MATH_AdivB:
		if(b != 0){
			math = a / b;
		}else if(a>0){
			math = 65535;
		}else if(a<0){
			math = -65535;
		}else{
			math = 0;
		}
		break;
	case MATH_BdivA:
		if(a != 0){
			math = b / a;
		}else if(b>0){
			math = 65535;
		}else if(b<0){
			math = -65535;
		}else{
			math = 0;
		}
		break;
	}
	return math;
}

void applySettings(volatile scopeConfig_t* config){
	//set timebase
	setSequencerParam(SCOPESEQUENCER_0_BASE + SCOPESEQUENCER_DIVIDER,
			timebasePrescallers[config->timeBase]);

	//channels enable and disable (affecting trigger & math)
	if(config->channelA.enabled != config->channelB.enabled){//only one channel enabled
		config->channelMath.enabled = 0; //disable math if only one channel enabled
		if(config->channelA.enabled){//only A enabled
			enableDisplay(SCOPEDISPLAY_0_BASE, SCOPEDISPLAY_ENABLE_G0);
			disableDisplay(SCOPEDISPLAY_0_BASE, SCOPEDISPLAY_ENABLE_G1);

			//both aquisition channels to CHA
			setSequencerParam(SCOPESEQUENCER_0_BASE + SCOPESEQUENCER_CHA, CHA_ADC_NUM);
			setSequencerParam(SCOPESEQUENCER_0_BASE + SCOPESEQUENCER_CHB, CHA_ADC_NUM);

			config->triggerSource = TRIG_CHA;
		}else{//only B enabled
			enableDisplay(SCOPEDISPLAY_0_BASE, SCOPEDISPLAY_ENABLE_G1);
			disableDisplay(SCOPEDISPLAY_0_BASE, SCOPEDISPLAY_ENABLE_G0);

			//both aquisition channels to CHB
			setSequencerParam(SCOPESEQUENCER_0_BASE + SCOPESEQUENCER_CHA, CHB_ADC_NUM);
			setSequencerParam(SCOPESEQUENCER_0_BASE + SCOPESEQUENCER_CHB, CHB_ADC_NUM);

			config->triggerSource = TRIG_CHB;
		}
	}else if(config->channelA.enabled){//both channels enabled
		enableDisplay(SCOPEDISPLAY_0_BASE, SCOPEDISPLAY_ENABLE_G0);
		enableDisplay(SCOPEDISPLAY_0_BASE, SCOPEDISPLAY_ENABLE_G1);

		setSequencerParam(SCOPESEQUENCER_0_BASE + SCOPESEQUENCER_CHA, CHA_ADC_NUM);
		setSequencerParam(SCOPESEQUENCER_0_BASE + SCOPESEQUENCER_CHB, CHB_ADC_NUM);

	}else{//both disabled, haha
		disableDisplay(SCOPEDISPLAY_0_BASE, SCOPEDISPLAY_ENABLE_G0);
		disableDisplay(SCOPEDISPLAY_0_BASE, SCOPEDISPLAY_ENABLE_G1);

		//both aquisition channels to CHA
		setSequencerParam(SCOPESEQUENCER_0_BASE + SCOPESEQUENCER_CHA, CHA_ADC_NUM);
		setSequencerParam(SCOPESEQUENCER_0_BASE + SCOPESEQUENCER_CHB, CHA_ADC_NUM);

		config->triggerSource = TRIG_CHA;
	}

	//enable or disable math
	if(config->channelMath.enabled){
		enableDisplay(SCOPEDISPLAY_0_BASE, SCOPEDISPLAY_ENABLE_G2);
	}else{
		disableDisplay(SCOPEDISPLAY_0_BASE, SCOPEDISPLAY_ENABLE_G2);
	}

	//set trigger source
	setSequencerParam(SCOPESEQUENCER_0_BASE + SCOPESEQUENCER_TRIGCH,
			config->triggerSource==TRIG_CHA?SCOPESEQUENCER_TRIGCH_CHA:SCOPESEQUENCER_TRIGCH_CHB);

	//set trigger edge
	setSequencerParam(SCOPESEQUENCER_0_BASE + SCOPESEQUENCER_TRIGEDGE,
			config->triggerEdge==TRIG_RISE?SCOPESEQUENCER_TRIGEDGE_R:SCOPESEQUENCER_TRIGEDGE_F);

	//set trigger level
	setSequencerParam(SCOPESEQUENCER_0_BASE + SCOPESEQUENCER_TRIGVAL, config->triggerVal+255);
	int32_t trigValTmp = config->triggerVal;


	if(config->triggerSource==TRIG_CHA){
		trigValTmp = applyGain(config->channelA.gain, trigValTmp);
		trigValTmp += config->channelA.offset;
	}else{
		trigValTmp = applyGain(config->channelB.gain, trigValTmp);
		trigValTmp += config->channelB.offset;
	}
	if(trigValTmp > 256) trigValTmp = 256;
	if(trigValTmp < -255) trigValTmp = -255;
	setDisplayParam(SCOPEDISPLAY_0_BASE + SCOPEDISPLAY_CFG_TRIGVAL, trigValTmp+255);

	//set trigger hyst
	setSequencerParam(SCOPESEQUENCER_0_BASE + SCOPESEQUENCER_HYSTVAL, config->triggerHyst);
}

void displaySettings(volatile scopeConfig_t* config){
	char text[100];
	sprintf(text, "CHA:");
	sprintf(text, "%s%c", text, config->activeMenu==0?'>':' ');
	if(config->channelA.enabled){
		sprintf(text,"%s ON", text);
	}else{
		sprintf(text,"%sOFF", text);
	}
	sprintf(text, "%s%c", text, config->activeMenu==1?'>':' ');
	sprintf(text, "%s%s", text, gainNames[config->channelA.gain]);
	sprintf(text, "%s%c", text, config->activeMenu==2?'>':' ');
	if(config->channelA.offset > 0){
		sprintf(text, "%s+%03ld", text, config->channelA.offset);
	}else{
		sprintf(text, "%s-%03ld", text, -config->channelA.offset);
	}

	sprintf(text, "%s CHB:", text);
	sprintf(text, "%s%c", text, config->activeMenu==3?'>':' ');
	if(config->channelB.enabled){
		sprintf(text,"%s ON", text);
	}else{
		sprintf(text,"%sOFF", text);
	}
	sprintf(text, "%s%c", text, config->activeMenu==4?'>':' ');
	sprintf(text, "%s%s", text, gainNames[config->channelB.gain]);
	sprintf(text, "%s%c", text, config->activeMenu==5?'>':' ');
	if(config->channelB.offset > 0){
		sprintf(text, "%s+%03ld", text, config->channelB.offset);
	}else{
		sprintf(text, "%s-%03ld", text, -config->channelB.offset);
	}

	sprintf(text, "%s MATH:", text);
	sprintf(text, "%s%c", text, config->activeMenu==6?'>':' ');
	sprintf(text, "%s%s", text, mathNames[config->mathMode]);
	sprintf(text, "%s%c", text, config->activeMenu==7?'>':' ');
	if(config->channelMath.enabled){
		sprintf(text, "%s ON", text);
	}else{
		sprintf(text, "%sOFF", text);
	}
	sprintf(text, "%s%c", text, config->activeMenu==8?'>':' ');
	sprintf(text, "%s%s", text, gainNames[config->channelMath.gain]);
	sprintf(text, "%s%c", text, config->activeMenu==9?'>':' ');
	if(config->channelMath.offset > 0){
		sprintf(text, "%s+%03ld", text, config->channelMath.offset);
	}else{
		sprintf(text, "%s-%03ld", text, -config->channelMath.offset);
	}

	sprintf(text, "%s TRIG:", text);
	sprintf(text, "%s%c", text, config->activeMenu==10?'>':' ');
	sprintf(text, "%s%c", text, trigSourceNames[config->triggerSource]);
	sprintf(text, "%s%c", text, config->activeMenu==11?'>':' ');
	sprintf(text, "%s%c", text, trigEdgeNames[config->triggerEdge]);

	sprintf(text, "%s%c", text, config->activeMenu==12?'>':' ');
	if(config->triggerVal > 0){
		sprintf(text, "%s+%03ld", text, config->triggerVal);
	}else{
		sprintf(text, "%s-%03ld", text, -config->triggerVal);
	}
	sprintf(text, "%s%c", text, config->activeMenu==13?'>':' ');
	sprintf(text, "%s/%03ld", text, config->triggerHyst);

	sprintf(text, "%s%c", text, config->activeMenu==14?'>':' ');
	sprintf(text, "%s%c", text, trigModeNames[config->triggerMode]);
	sprintf(text, "%s%c", text, config->activeMenu==15?'>':' ');
	sprintf(text, "%s%c", text, trigStateNames[config->triggerState]);

	sprintf(text, "%s TIM:", text);

	sprintf(text, "%s%c", text, config->activeMenu==16?'>':' ');
	sprintf(text, "%s%s", text, timebaseNames[config->timeBase]);

	putString(text, 0);
}

void menuPlus(uint8_t param){
	switch(scopeConfig.activeMenu){
	case 0://CHA on/off
		scopeConfig.channelA.enabled = !scopeConfig.channelA.enabled;
		break;
	case 1://CHA gain
		if(scopeConfig.channelA.gain < GAIN_10) scopeConfig.channelA.gain++;
		break;
	case 2://CHA offset
		if(scopeConfig.channelA.offset < 256) scopeConfig.channelA.offset++;
		break;
	case 3://CHB on/off
		scopeConfig.channelB.enabled = !scopeConfig.channelB.enabled;
		break;
	case 4://CHB gain
		if(scopeConfig.channelB.gain < GAIN_10) scopeConfig.channelB.gain++;
		break;
	case 5://CHB offset
		if(scopeConfig.channelB.offset < 256) scopeConfig.channelB.offset++;
		break;
	case 6://math mode
		if(scopeConfig.mathMode < MATH_AmultB){
			scopeConfig.mathMode++;
		}else{
			scopeConfig.mathMode = MATH_AplusB;
		}
		break;
	case 7://math on/off
		scopeConfig.channelMath.enabled = !scopeConfig.channelMath.enabled;
		break;
	case 8://math gain
		if(scopeConfig.channelMath.gain < GAIN_10) scopeConfig.channelMath.gain++;
		break;
	case 9://math offset
		if(scopeConfig.channelMath.offset < 256) scopeConfig.channelMath.offset++;
		break;
	case 10://trig source
		scopeConfig.triggerSource = !scopeConfig.triggerSource;
		break;
	case 11://trig edge
		scopeConfig.triggerEdge = !scopeConfig.triggerEdge;
		break;
	case 12://trig val
		if(scopeConfig.triggerVal < 256) scopeConfig.triggerVal++;
		break;
	case 13://trig hyst
		if(scopeConfig.triggerHyst < 128) scopeConfig.triggerHyst++;
		break;
	case 14://trig mode
		if(scopeConfig.triggerMode < TRIG_SINGLE){
			scopeConfig.triggerMode++;
		}else{
			scopeConfig.triggerMode = TRIG_NORMAL;
		}
		scopeConfig.triggerState = TRIG_READY;
		triggerArm(SCOPESEQUENCER_0_BASE);
		break;
	case 15://trig state
		switch(scopeConfig.triggerMode){
			case TRIG_AUTO:
			case TRIG_NORMAL:
				if(scopeConfig.triggerState == TRIG_STOP){
					scopeConfig.triggerState = TRIG_READY;
					triggerArm(SCOPESEQUENCER_0_BASE);
				}else{
					scopeConfig.triggerState = TRIG_STOP;
				}
				break;
			case TRIG_SINGLE:
				scopeConfig.triggerState = TRIG_READY;
				triggerArm(SCOPESEQUENCER_0_BASE);
				break;
		}
		break;
	case 16://time base
		if(scopeConfig.timeBase < TIMEBASE_100) scopeConfig.timeBase++;
		break;
	}

	scopeConfig.triggerAuto = triggerAutos[scopeConfig.timeBase];
	scopeConfig.timeout = scopeConfig.triggerAuto;

	applySettings(&scopeConfig);
	displaySettings(&scopeConfig);
}

void menuMinus(uint8_t param){
	switch(scopeConfig.activeMenu){
	case 0://CHA on/off
		scopeConfig.channelA.enabled = !scopeConfig.channelA.enabled;
		break;
	case 1://CHA gain
		if(scopeConfig.channelA.gain > GAIN_01) scopeConfig.channelA.gain--;
		break;
	case 2://CHA offset
		if(scopeConfig.channelA.offset > -255) scopeConfig.channelA.offset--;
		break;
	case 3://CHB on/off
		scopeConfig.channelB.enabled = !scopeConfig.channelB.enabled;
		break;
	case 4://CHB gain
		if(scopeConfig.channelB.gain > GAIN_01) scopeConfig.channelB.gain--;
		break;
	case 5://CHB offset
		if(scopeConfig.channelB.offset > -255) scopeConfig.channelB.offset--;
		break;
	case 6://math mode
		if(scopeConfig.mathMode > MATH_AplusB){
			scopeConfig.mathMode--;
		}else{
			scopeConfig.mathMode = MATH_AmultB;
		}
		break;
	case 7://math on/off
		scopeConfig.channelMath.enabled = !scopeConfig.channelMath.enabled;
		break;
	case 8://math gain
		if(scopeConfig.channelMath.gain > GAIN_01) scopeConfig.channelMath.gain--;
		break;
	case 9://math offset
		if(scopeConfig.channelMath.offset > -255) scopeConfig.channelMath.offset--;
		break;
	case 10://trig source
		scopeConfig.triggerSource = !scopeConfig.triggerSource;
		break;
	case 11://trig edge
		scopeConfig.triggerEdge = !scopeConfig.triggerEdge;
		break;
	case 12://trig val
		if(scopeConfig.triggerVal > -255) scopeConfig.triggerVal--;
		break;
	case 13://trig hyst
		if(scopeConfig.triggerHyst > 0) scopeConfig.triggerHyst--;
		break;
	case 14://trig mode
		if(scopeConfig.triggerMode > TRIG_NORMAL){
			scopeConfig.triggerMode--;
		}else{
			scopeConfig.triggerMode = TRIG_SINGLE ;
		}
		scopeConfig.triggerState = TRIG_READY;
		break;
	case 15://trig state
		switch(scopeConfig.triggerMode){
			case TRIG_AUTO:
			case TRIG_NORMAL:
				if(scopeConfig.triggerState == TRIG_STOP){
					scopeConfig.triggerState = TRIG_READY;
					triggerArm(SCOPESEQUENCER_0_BASE);
				}else{
					scopeConfig.triggerState = TRIG_STOP;
				}
				break;
			case TRIG_SINGLE:
				scopeConfig.triggerState = TRIG_READY;
				triggerArm(SCOPESEQUENCER_0_BASE);
				break;
		}
		break;
	case 16://time base
		if(scopeConfig.timeBase > TIMEBASE_01) scopeConfig.timeBase--;
		break;
	}

	scopeConfig.triggerAuto = triggerAutos[scopeConfig.timeBase];
	scopeConfig.timeout = scopeConfig.triggerAuto;

	applySettings(&scopeConfig);
	displaySettings(&scopeConfig);
}

void menuClick(uint8_t param){
	scopeConfig.activeMenu++;
	if(scopeConfig.activeMenu > 16) scopeConfig.activeMenu = 0;
	displaySettings(&scopeConfig);
}
