#include "SequencerDriver.h"

int32_t readData(uint32_t base, uint32_t offset){
	int32_t x = IORD_32DIRECT(base + SCOPESEQUENCER_DATA_BASE + 4 * offset, 0);
	return x-255;
}

void triggerArm(uint32_t base){
	IOWR_32DIRECT(base + SCOPESEQUENCER_TRIGARM, 0, SCOPESEQUENCER_TRIGARM_ARM);
}

void triggerForce(uint32_t base){
	IOWR_32DIRECT(base + SCOPESEQUENCER_TRIGFORCE, 0, SCOPESEQUENCER_TRIGFORCE_FORCE);
}

void triggerUnForce(uint32_t base){
	IOWR_32DIRECT(base + SCOPESEQUENCER_TRIGFORCE, 0, SCOPESEQUENCER_TRIGFORCE_IDLE);
}

void resetIRQ(uint32_t base){
	IOWR_32DIRECT(base + SCOPESEQUENCER_IRQ, 0, 0);
}

void setSequencerParam(uint32_t base, uint32_t param){
	IOWR_32DIRECT(base, 0, param);
}
