#ifndef KBD_H_
#define KBD_H_

#include <stdint.h>

//definicje alias�w dla przycisk�w
#define KBD_NONE	255
#define KBD_OK		0
#define KBD_UP		1
#define KBD_DN		2

#define KBD_PIO_BASE BTN_IO_BASE

#define BTN_DN_M (1<<0)
#define BTN_UP_M (1<<1)
#define BTN_OK_M (1<<2)


//minimalny czas dobouncingu (podawany w ms)
#define CLICK_MIN 50
//minimalny czas przytrzymania
#define HOLD_MIN 500
//powtarzanie po przytrzymaniu
#define HOLD_REP 70


//struktura elementu menu ze wska�nikiem na funkcj� oraz parametrem jej wywo�ania
typedef struct
{
	void (*action)(uint8_t);
	uint8_t parameter;
}menu_action_t;

//struktura uwzgl�dniaj��a akcj� dla klikni�cia i przytrzymania
typedef struct
{
	menu_action_t click;
	menu_action_t hold;
}menu_item_t;

//tabela struktur - dla ka�dego przycisku
menu_item_t menu_items[3];

void kbdCheck(void);


#endif /* KBD_H_ */
