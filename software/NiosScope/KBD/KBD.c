#include "system.h"
#include "sys/alt_stdio.h"
#include "sys/alt_sys_wrappers.h"
#include "altera_avalon_pio_regs.h"

#include "KBD.h"

uint8_t kbd_read(void){
	uint16_t actual = IORD_ALTERA_AVALON_PIO_DATA(KBD_PIO_BASE);
	if(!(actual & BTN_DN_M)) return KBD_DN;
	if(!(actual & BTN_UP_M)) return KBD_UP;
	if(!(actual & BTN_OK_M)) return KBD_OK;
	return KBD_NONE;
}

void kbdCheck(void){
	static uint8_t kbd_time=0;
	static uint8_t last_kbd;
	static enum {KBD_IDLE, KBD_DEBOUNCE_WAIT, KBD_HOLD_WAIT, KBD_HOLD} kbd_state;


	uint8_t act_kbd = kbd_read(); //  odczytujemy wci�ni�ty przycisk

	if(kbd_time) kbd_time--; //  je�li trzeba odmierza� czas to go odmierzamy

	if(act_kbd == KBD_NONE){ //  je�li wszystkie przyciski puszczono
		if(kbd_state == KBD_HOLD_WAIT){ //  i by� odmierzony czas klikni�cia
			if(menu_items[last_kbd].click.action)
				menu_items[last_kbd].click.action(menu_items[last_kbd].click.parameter); //  to wywo�ujemy funkcj� klikni�cia
		}
		kbd_state = KBD_IDLE; //  i resetujemy system
	}else{ //  w przeciwnym wypadku
		if(kbd_state == KBD_IDLE){ //  je�li to nowe przyci�ni�cie
			kbd_state = KBD_DEBOUNCE_WAIT; //  przechodzimy do fazy redukowania drga� styk�w
			kbd_time = CLICK_MIN; //  ustawiamy czas redukcji drga�
			last_kbd = act_kbd; //  i zapami�tujemy aktualnie wci�ni�ty przycisk
		}else if(kbd_state == KBD_DEBOUNCE_WAIT){ //  je�li jeste�my w trybie eleminiacji drga� styk�w
			if(last_kbd == act_kbd){ //  je�li nadal ten sam przycisk
				if(kbd_time == 0){ //  je�li ju� up�yn�� ca�y czas
					kbd_state = KBD_HOLD_WAIT; //  to przechodzimy do opcji wyzwalania klikni�cia i odmierzenia czasu trzymania
					kbd_time = HOLD_MIN; //  ustawiamy czas przytrzymania
				}
			}else{ //  je�li zmieni� si� przycisk
				kbd_state = KBD_IDLE; //  to resetujemy system
			}
		}else if(kbd_state == KBD_HOLD_WAIT){ //  je�li oczekujemy na przytrzymanie
			if(last_kbd == act_kbd){ //  je�li nadal ten sam przycisk
				if(kbd_time == 0){ //  je�li ju� up�yn�� ca�y czas
					kbd_state = KBD_HOLD; //  to przechodzimy do opcji powtarzania klikni��
					kbd_time = HOLD_REP; //  ustawiamy czas przytrzymania
					if(menu_items[last_kbd].hold.action)
						menu_items[last_kbd].hold.action(menu_items[last_kbd].hold.parameter); //  to wywo�ujemy funkcj� przytrzymania
				}
			}else{ //  je�li zmieni� si� przycisk
				kbd_state = KBD_IDLE; //  to resetujemy system
			}
		}else if(kbd_state == KBD_HOLD){
			if(last_kbd == act_kbd){ //  je�li nadal ten sam przycisk
				if(kbd_time == 0){ //  je�li ju� up�yn�� ca�y czas
					kbd_state = KBD_HOLD; //  to przechodzimy do opcji powtarzania klikni��
					kbd_time = HOLD_REP; //  ustawiamy czas przytrzymania
					if(menu_items[last_kbd].hold.action)
						menu_items[last_kbd].hold.action(menu_items[last_kbd].hold.parameter); //  to wywo�ujemy funkcj� przytrzymania
				}
			}else{ //  je�li zmieni� si� przycisk
				kbd_state = KBD_IDLE; //  to resetujemy system
			}
		}
	}
}
