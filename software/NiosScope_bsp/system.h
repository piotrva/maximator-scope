/*
 * system.h - SOPC Builder system and BSP software package information
 *
 * Machine generated for CPU 'CPU' in SOPC Builder design 'NiosScope'
 * SOPC Builder design path: ../../NiosScope.sopcinfo
 *
 * Generated: Fri Sep 14 22:01:43 CEST 2018
 */

/*
 * DO NOT MODIFY THIS FILE
 *
 * Changing this file will have subtle consequences
 * which will almost certainly lead to a nonfunctioning
 * system. If you do modify this file, be aware that your
 * changes will be overwritten and lost when this file
 * is generated again.
 *
 * DO NOT MODIFY THIS FILE
 */

/*
 * License Agreement
 *
 * Copyright (c) 2008
 * Altera Corporation, San Jose, California, USA.
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * This agreement shall be governed in all respects by the laws of the State
 * of California and by the laws of the United States of America.
 */

#ifndef __SYSTEM_H_
#define __SYSTEM_H_

/* Include definitions from linker script generator */
#include "linker.h"


/*
 * BTN_IO configuration
 *
 */

#define ALT_MODULE_CLASS_BTN_IO altera_avalon_pio
#define BTN_IO_BASE 0x1d040
#define BTN_IO_BIT_CLEARING_EDGE_REGISTER 0
#define BTN_IO_BIT_MODIFYING_OUTPUT_REGISTER 0
#define BTN_IO_CAPTURE 0
#define BTN_IO_DATA_WIDTH 3
#define BTN_IO_DO_TEST_BENCH_WIRING 0
#define BTN_IO_DRIVEN_SIM_VALUE 0
#define BTN_IO_EDGE_TYPE "NONE"
#define BTN_IO_FREQ 50000000
#define BTN_IO_HAS_IN 1
#define BTN_IO_HAS_OUT 0
#define BTN_IO_HAS_TRI 0
#define BTN_IO_IRQ -1
#define BTN_IO_IRQ_INTERRUPT_CONTROLLER_ID -1
#define BTN_IO_IRQ_TYPE "NONE"
#define BTN_IO_NAME "/dev/BTN_IO"
#define BTN_IO_RESET_VALUE 0
#define BTN_IO_SPAN 16
#define BTN_IO_TYPE "altera_avalon_pio"


/*
 * CPU configuration
 *
 */

#define ALT_CPU_ARCHITECTURE "altera_nios2_gen2"
#define ALT_CPU_BIG_ENDIAN 0
#define ALT_CPU_BREAK_ADDR 0x0001c820
#define ALT_CPU_CPU_ARCH_NIOS2_R1
#define ALT_CPU_CPU_FREQ 50000000u
#define ALT_CPU_CPU_ID_SIZE 1
#define ALT_CPU_CPU_ID_VALUE 0x00000000
#define ALT_CPU_CPU_IMPLEMENTATION "tiny"
#define ALT_CPU_DATA_ADDR_WIDTH 0x11
#define ALT_CPU_DCACHE_LINE_SIZE 0
#define ALT_CPU_DCACHE_LINE_SIZE_LOG2 0
#define ALT_CPU_DCACHE_SIZE 0
#define ALT_CPU_EXCEPTION_ADDR 0x00008020
#define ALT_CPU_FLASH_ACCELERATOR_LINES 0
#define ALT_CPU_FLASH_ACCELERATOR_LINE_SIZE 0
#define ALT_CPU_FLUSHDA_SUPPORTED
#define ALT_CPU_FREQ 50000000
#define ALT_CPU_HARDWARE_DIVIDE_PRESENT 0
#define ALT_CPU_HARDWARE_MULTIPLY_PRESENT 0
#define ALT_CPU_HARDWARE_MULX_PRESENT 0
#define ALT_CPU_HAS_DEBUG_CORE 1
#define ALT_CPU_HAS_DEBUG_STUB
#define ALT_CPU_HAS_ILLEGAL_INSTRUCTION_EXCEPTION
#define ALT_CPU_HAS_JMPI_INSTRUCTION
#define ALT_CPU_ICACHE_LINE_SIZE 0
#define ALT_CPU_ICACHE_LINE_SIZE_LOG2 0
#define ALT_CPU_ICACHE_SIZE 0
#define ALT_CPU_INST_ADDR_WIDTH 0x11
#define ALT_CPU_NAME "CPU"
#define ALT_CPU_OCI_VERSION 1
#define ALT_CPU_RESET_ADDR 0x00008000


/*
 * CPU configuration (with legacy prefix - don't use these anymore)
 *
 */

#define NIOS2_BIG_ENDIAN 0
#define NIOS2_BREAK_ADDR 0x0001c820
#define NIOS2_CPU_ARCH_NIOS2_R1
#define NIOS2_CPU_FREQ 50000000u
#define NIOS2_CPU_ID_SIZE 1
#define NIOS2_CPU_ID_VALUE 0x00000000
#define NIOS2_CPU_IMPLEMENTATION "tiny"
#define NIOS2_DATA_ADDR_WIDTH 0x11
#define NIOS2_DCACHE_LINE_SIZE 0
#define NIOS2_DCACHE_LINE_SIZE_LOG2 0
#define NIOS2_DCACHE_SIZE 0
#define NIOS2_EXCEPTION_ADDR 0x00008020
#define NIOS2_FLASH_ACCELERATOR_LINES 0
#define NIOS2_FLASH_ACCELERATOR_LINE_SIZE 0
#define NIOS2_FLUSHDA_SUPPORTED
#define NIOS2_HARDWARE_DIVIDE_PRESENT 0
#define NIOS2_HARDWARE_MULTIPLY_PRESENT 0
#define NIOS2_HARDWARE_MULX_PRESENT 0
#define NIOS2_HAS_DEBUG_CORE 1
#define NIOS2_HAS_DEBUG_STUB
#define NIOS2_HAS_ILLEGAL_INSTRUCTION_EXCEPTION
#define NIOS2_HAS_JMPI_INSTRUCTION
#define NIOS2_ICACHE_LINE_SIZE 0
#define NIOS2_ICACHE_LINE_SIZE_LOG2 0
#define NIOS2_ICACHE_SIZE 0
#define NIOS2_INST_ADDR_WIDTH 0x11
#define NIOS2_OCI_VERSION 1
#define NIOS2_RESET_ADDR 0x00008000


/*
 * CPU_ID configuration
 *
 */

#define ALT_MODULE_CLASS_CPU_ID altera_avalon_sysid_qsys
#define CPU_ID_BASE 0x1d070
#define CPU_ID_ID 0
#define CPU_ID_IRQ -1
#define CPU_ID_IRQ_INTERRUPT_CONTROLLER_ID -1
#define CPU_ID_NAME "/dev/CPU_ID"
#define CPU_ID_SPAN 8
#define CPU_ID_TIMESTAMP 1536954283
#define CPU_ID_TYPE "altera_avalon_sysid_qsys"


/*
 * Define for each module class mastered by the CPU
 *
 */

#define __ALTERA_AVALON_ONCHIP_MEMORY2
#define __ALTERA_AVALON_PIO
#define __ALTERA_AVALON_SYSID_QSYS
#define __ALTERA_AVALON_TIMER
#define __ALTERA_AVALON_UART
#define __ALTERA_NIOS2_GEN2
#define __ALTPLL
#define __SCOPEDISPLAY
#define __SCOPESEQUENCER


/*
 * LED_IO configuration
 *
 */

#define ALT_MODULE_CLASS_LED_IO altera_avalon_pio
#define LED_IO_BASE 0x1d050
#define LED_IO_BIT_CLEARING_EDGE_REGISTER 0
#define LED_IO_BIT_MODIFYING_OUTPUT_REGISTER 0
#define LED_IO_CAPTURE 0
#define LED_IO_DATA_WIDTH 4
#define LED_IO_DO_TEST_BENCH_WIRING 0
#define LED_IO_DRIVEN_SIM_VALUE 0
#define LED_IO_EDGE_TYPE "NONE"
#define LED_IO_FREQ 50000000
#define LED_IO_HAS_IN 0
#define LED_IO_HAS_OUT 1
#define LED_IO_HAS_TRI 0
#define LED_IO_IRQ -1
#define LED_IO_IRQ_INTERRUPT_CONTROLLER_ID -1
#define LED_IO_IRQ_TYPE "NONE"
#define LED_IO_NAME "/dev/LED_IO"
#define LED_IO_RESET_VALUE 0
#define LED_IO_SPAN 16
#define LED_IO_TYPE "altera_avalon_pio"


/*
 * MEMORY configuration
 *
 */

#define ALT_MODULE_CLASS_MEMORY altera_avalon_onchip_memory2
#define MEMORY_ALLOW_IN_SYSTEM_MEMORY_CONTENT_EDITOR 0
#define MEMORY_ALLOW_MRAM_SIM_CONTENTS_ONLY_FILE 0
#define MEMORY_BASE 0x8000
#define MEMORY_CONTENTS_INFO ""
#define MEMORY_DUAL_PORT 0
#define MEMORY_GUI_RAM_BLOCK_TYPE "AUTO"
#define MEMORY_INIT_CONTENTS_FILE "NiosScope_MEMORY"
#define MEMORY_INIT_MEM_CONTENT 1
#define MEMORY_INSTANCE_ID "NONE"
#define MEMORY_IRQ -1
#define MEMORY_IRQ_INTERRUPT_CONTROLLER_ID -1
#define MEMORY_NAME "/dev/MEMORY"
#define MEMORY_NON_DEFAULT_INIT_FILE_ENABLED 1
#define MEMORY_RAM_BLOCK_TYPE "AUTO"
#define MEMORY_READ_DURING_WRITE_MODE "DONT_CARE"
#define MEMORY_SINGLE_CLOCK_OP 0
#define MEMORY_SIZE_MULTIPLE 1
#define MEMORY_SIZE_VALUE 32768
#define MEMORY_SPAN 32768
#define MEMORY_TYPE "altera_avalon_onchip_memory2"
#define MEMORY_WRITABLE 1


/*
 * PLL0 configuration
 *
 */

#define ALT_MODULE_CLASS_PLL0 altpll
#define PLL0_BASE 0x1d060
#define PLL0_IRQ -1
#define PLL0_IRQ_INTERRUPT_CONTROLLER_ID -1
#define PLL0_NAME "/dev/PLL0"
#define PLL0_SPAN 16
#define PLL0_TYPE "altpll"


/*
 * System configuration
 *
 */

#define ALT_DEVICE_FAMILY "MAX 10"
#define ALT_ENHANCED_INTERRUPT_API_PRESENT
#define ALT_IRQ_BASE NULL
#define ALT_LOG_PORT "/dev/null"
#define ALT_LOG_PORT_BASE 0x0
#define ALT_LOG_PORT_DEV null
#define ALT_LOG_PORT_TYPE ""
#define ALT_NUM_EXTERNAL_INTERRUPT_CONTROLLERS 0
#define ALT_NUM_INTERNAL_INTERRUPT_CONTROLLERS 1
#define ALT_NUM_INTERRUPT_CONTROLLERS 1
#define ALT_STDERR "/dev/UART"
#define ALT_STDERR_BASE 0x1d000
#define ALT_STDERR_DEV UART
#define ALT_STDERR_IS_UART
#define ALT_STDERR_PRESENT
#define ALT_STDERR_TYPE "altera_avalon_uart"
#define ALT_STDIN "/dev/UART"
#define ALT_STDIN_BASE 0x1d000
#define ALT_STDIN_DEV UART
#define ALT_STDIN_IS_UART
#define ALT_STDIN_PRESENT
#define ALT_STDIN_TYPE "altera_avalon_uart"
#define ALT_STDOUT "/dev/UART"
#define ALT_STDOUT_BASE 0x1d000
#define ALT_STDOUT_DEV UART
#define ALT_STDOUT_IS_UART
#define ALT_STDOUT_PRESENT
#define ALT_STDOUT_TYPE "altera_avalon_uart"
#define ALT_SYSTEM_NAME "NiosScope"


/*
 * TIM0 configuration
 *
 */

#define ALT_MODULE_CLASS_TIM0 altera_avalon_timer
#define TIM0_ALWAYS_RUN 0
#define TIM0_BASE 0x1d020
#define TIM0_COUNTER_SIZE 32
#define TIM0_FIXED_PERIOD 0
#define TIM0_FREQ 50000000
#define TIM0_IRQ 0
#define TIM0_IRQ_INTERRUPT_CONTROLLER_ID 0
#define TIM0_LOAD_VALUE 49999
#define TIM0_MULT 2.0E-8
#define TIM0_NAME "/dev/TIM0"
#define TIM0_PERIOD 50000
#define TIM0_PERIOD_UNITS "clocks"
#define TIM0_RESET_OUTPUT 0
#define TIM0_SNAPSHOT 1
#define TIM0_SPAN 32
#define TIM0_TICKS_PER_SEC 1000
#define TIM0_TIMEOUT_PULSE_OUTPUT 0
#define TIM0_TYPE "altera_avalon_timer"


/*
 * UART configuration
 *
 */

#define ALT_MODULE_CLASS_UART altera_avalon_uart
#define UART_BASE 0x1d000
#define UART_BAUD 115200
#define UART_DATA_BITS 8
#define UART_FIXED_BAUD 0
#define UART_FREQ 50000000
#define UART_IRQ 1
#define UART_IRQ_INTERRUPT_CONTROLLER_ID 0
#define UART_NAME "/dev/UART"
#define UART_PARITY 'N'
#define UART_SIM_CHAR_STREAM ""
#define UART_SIM_TRUE_BAUD 0
#define UART_SPAN 32
#define UART_STOP_BITS 1
#define UART_SYNC_REG_DEPTH 2
#define UART_TYPE "altera_avalon_uart"
#define UART_USE_CTS_RTS 0
#define UART_USE_EOP_REGISTER 0


/*
 * hal configuration
 *
 */

#define ALT_INCLUDE_INSTRUCTION_RELATED_EXCEPTION_API
#define ALT_MAX_FD 4
#define ALT_SYS_CLK none
#define ALT_TIMESTAMP_CLK none


/*
 * scopeDisplay_0 configuration
 *
 */

#define ALT_MODULE_CLASS_scopeDisplay_0 scopeDisplay
#define SCOPEDISPLAY_0_BASE 0x10000
#define SCOPEDISPLAY_0_IRQ -1
#define SCOPEDISPLAY_0_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SCOPEDISPLAY_0_NAME "/dev/scopeDisplay_0"
#define SCOPEDISPLAY_0_SPAN 32768
#define SCOPEDISPLAY_0_TYPE "scopeDisplay"


/*
 * scopeSequencer_0 configuration
 *
 */

#define ALT_MODULE_CLASS_scopeSequencer_0 scopeSequencer
#define SCOPESEQUENCER_0_BASE 0x18000
#define SCOPESEQUENCER_0_IRQ 2
#define SCOPESEQUENCER_0_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SCOPESEQUENCER_0_NAME "/dev/scopeSequencer_0"
#define SCOPESEQUENCER_0_SPAN 16384
#define SCOPESEQUENCER_0_TYPE "scopeSequencer"

#endif /* __SYSTEM_H_ */
