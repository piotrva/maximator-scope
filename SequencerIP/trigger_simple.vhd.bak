-- Project:			maximator-vga-oscilloscope
-- File:				trigger_simple.vhd
-- Version:			1.0 (13.05.2016)
-- Author:			Piotr Rzeszut (http://piotr94.net21.pl)
-- Description:	Processes trigger and controls display memory write

library ieee ;

use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity trigger_simple is
	port(
		clk					: in  std_logic;
		trigger				: in  std_logic;
		wraddress 			: out std_logic_vector(9 downto 0);
		wr_en					: out std_logic
	);
end trigger_simple;

architecture basic of trigger_simple is
	signal prescaller			: std_logic_vector(31 downto 0);
	signal wraddress_int 	: std_logic_vector(9 downto 0);
	
	type trigger_state_t is (trigger_idle, trigger_copy);
	signal trigger_state : trigger_state_t;
begin

	wraddress <= wraddress_int;

	process(clk) is
	begin
		if falling_edge(clk) then
				case trigger_state is
					when trigger_idle =>
						if trigger = '0' then
							trigger_state <= trigger_copy;
							wraddress_int <= (others => '0');
							wr_en <= '1';
						else
							trigger_state <= trigger_idle;
							wr_en <= '0';
						end if;
					when trigger_copy =>
						if prescaller >= std_logic_vector(to_unsigned(500,32)) then
							prescaller <= (others => '0');
							if wraddress_int = "1111111111" then
								trigger_state <= trigger_idle;
								wr_en <= '1';
							else
								trigger_state <= trigger_copy;
								wraddress_int <= wraddress_int + 1;
								wr_en <= '1';
							end if;
						else
							wr_en <= '0';
							prescaller <= prescaller +1;
						end if;
				end case;
		end if;
	end process;

end basic;