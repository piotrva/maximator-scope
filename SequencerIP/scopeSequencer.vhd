-- Project:			maximator-NiosScope
-- File:				scopeSequencer.vhd
-- Version:			1.0 (14.04.2017)
-- Author:			Piotr Rzeszut (http://piotr94.net21.pl)
-- Description:	Gets samples form adc and triggers storage

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;


ENTITY scopeSequencer IS
	PORT (	
				--avalon Memory-Mapped slave
				clk			: IN STD_LOGIC;
				reset_n		: IN STD_LOGIC;
				address		: IN STD_LOGIC_VECTOR(11 DOWNTO 0);
				byteenable	: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
				read			: IN STD_LOGIC;
				readdata		: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
				write			: IN STD_LOGIC;
				writedata	: IN STD_LOGIC_VECTOR(31 DOWNTO 0);
				irq			: OUT STD_LOGIC;
				
				--avalon STreaming sink
				response_valid				: in  std_logic;                                       
				response_channel			: in  std_logic_vector(4 downto 0);                    
				response_data				: in  std_logic_vector(11 downto 0);                   
				response_startofpacket	: in  std_logic;                                     
				response_endofpacket		: in  std_logic;
				
				--avalon STreaming source
				command_channel 			: out std_logic_vector(4 downto 0);
				command_valid 				: out std_logic;
				command_startofpacket 	: out std_logic;
				command_endofpacket 		: out std_logic;
				command_ready 				: in std_logic
		);
END scopeSequencer;


ARCHITECTURE Structure OF scopeSequencer IS
	component adcSampling is
		port(
			clk							: in  std_logic;
			response_valid				: in  std_logic;                                       
			response_channel			: in  std_logic_vector(4 downto 0);                    
			response_data				: in  std_logic_vector(11 downto 0);                   
			response_startofpacket	: in  std_logic;                                     
			response_endofpacket		: in  std_logic;
			command_channel 			: out std_logic_vector(4 downto 0);
			command_valid 				: out std_logic;
			command_startofpacket 	: out std_logic;
			command_endofpacket 		: out std_logic;
			command_ready 				: in std_logic;
			channel_CHA					: in std_logic_vector(4 downto 0);
			channel_CHB					: in std_logic_vector(4 downto 0);
			valid_data_CHA				: out std_logic_vector(11 downto 0);
			valid_data_CHB				: out std_logic_vector(11 downto 0)
		);
	end component adcSampling;
	
	component trigger_simple is
		port(
			clk					: in  std_logic;
			trigger				: in  std_logic;
			is_idle				: out std_logic;
			is_done				: out std_logic;
			clr_done				: in std_logic;
			divider				: in std_logic_vector(31 downto 0);
			wraddress 			: out std_logic_vector(9 downto 0);
			wr_en					: out std_logic
		);
	end component trigger_simple;
	
	component trigger_hyst is 
		port(
			clk			: in	std_logic;
			rst_n			: in	std_logic;
			trigger_val	: in	std_logic_vector(10 downto 0);
			hyst_val		: in	std_logic_vector(10 downto 0);
			act_val		: in	std_logic_vector(10 downto 0);
			trigger_r	: out	std_logic;
			trigger_f	: out	std_logic
		);
	end component trigger_hyst;
	
	component SequencerRAM
		PORT
		(
			clock		: IN STD_LOGIC  := '1';
			data		: IN STD_LOGIC_VECTOR (8 DOWNTO 0);
			rdaddress		: IN STD_LOGIC_VECTOR (9 DOWNTO 0);
			wraddress		: IN STD_LOGIC_VECTOR (9 DOWNTO 0);
			wren		: IN STD_LOGIC  := '0';
			q		: OUT STD_LOGIC_VECTOR (8 DOWNTO 0)
		);
	end component;
	
	signal valid_dataA	:std_logic_vector(11 downto 0);
	signal valid_dataB	:std_logic_vector(11 downto 0);
	signal valid_dataT	:std_logic_vector(11 downto 0);
	signal valid_data		:std_logic_vector(11 downto 0);
	signal wraddress		:std_logic_vector(9 downto 0);
	signal wr_en			:std_logic;
	
	
	signal trigger_val	:std_logic_vector(10 downto 0);
	signal hyst_val		:std_logic_vector(10 downto 0);
	
	signal trigger_int	:std_logic;
	signal trigger_intr	:std_logic;
	signal trigger_intf	:std_logic;
	
	signal RAMdata			:std_logic_vector(31 downto 0);
	signal REGdata			:std_logic_vector(31 downto 0);
	
	signal channel_CHA	:std_logic_vector(4 downto 0);
	signal channel_CHB	:std_logic_vector(4 downto 0);
	
	signal trig_channel	:std_logic;
	signal trig_edge		:std_logic;
	signal trig_armed		:std_logic;
	signal trig_irq		:std_logic;
	signal trig_force		:std_logic;
	
	signal is_done			:std_logic;
	signal is_idle			:std_logic;
	signal clr_done		:std_logic;
	
	signal trig_divider	:std_logic_vector(31 downto 0);
BEGIN

	irq <= trig_irq;
	
	RAMdata(31 downto 9) <= (others => '0');
	
	readdata <=	RAMdata when address(11 downto 10) = "00" else
					REGdata when address(11 downto 10) = "01" else
					(others => '0');
	
	valid_dataT <= valid_dataA when trig_channel = '1' else
						valid_dataB;
	trigger_int <=	trigger_intr when trig_edge = '1' else
						trigger_intf;
	
	------------------------------------------------------------------
	process(clk, reset_n) is
	begin
		if reset_n ='0' then
			trigger_val 	<= std_logic_vector(to_unsigned(256,trigger_val'length));
			hyst_val 		<= std_logic_vector(to_unsigned(10,hyst_val'length));
			trig_irq			<= '0';
			trig_channel	<= '0';
			trig_edge		<= '0';
			trig_armed		<= '0';
			trig_force		<= '0';
			channel_CHA		<= "00110";
			channel_CHB		<= "00111";
			clr_done			<= '0';
			trig_divider	<= std_logic_vector(to_unsigned(390,trig_divider'length));
		elsif rising_edge(clk) then
			if write = '1' then
				if address = "010000000000" then
					trigger_val <= writedata(10 downto 0);
				elsif address = "010000000001" then
					hyst_val <= writedata(10 downto 0);
				elsif address = "010000000010" then
					trig_irq <= writedata(0);
				elsif address = "010000000011" then
					trig_channel <= writedata(0);
				elsif address = "010000000100" then
					trig_edge <= writedata(0);
				elsif address = "010000000101" then
					trig_armed <= writedata(0);
				elsif address = "010000000110" then
					trig_force <= writedata(0);
				elsif address = "010000000111" then
					channel_CHA <= writedata(4 downto 0);
				elsif address = "010000001000" then
					channel_CHB <= writedata(4 downto 0);
				elsif address = "010000001001" then
					trig_divider <= writedata;
				end if;
			elsif read = '1' then
				if address = "010000000000" then
					REGdata(10 downto 0) <= trigger_val;
					REGdata(31 downto 11) <= (others => '0');
				elsif address = "010000000001" then
					REGdata(10 downto 0) <= hyst_val;
					REGdata(31 downto 11) <= (others => '0');
				elsif address = "010000000010" then
					REGdata <= (0 => trig_irq, others => '0');
				elsif address = "010000000011" then
					REGdata <= (0 => trig_channel, others => '0');
				elsif address = "010000000100" then
					REGdata <= (0 => trig_edge, others => '0');
				elsif address = "010000000101" then
					REGdata <= (0 => trig_armed, others => '0');
				elsif address = "010000000110" then
					REGdata <= (0 => trig_force, others => '0');
				elsif address = "010000000111" then
					REGdata(4 downto 0) <= channel_CHA;
					REGdata(31 downto 5) <= (others => '0');
				elsif address = "010000001000" then
					REGdata(4 downto 0) <= channel_CHB;
					REGdata(31 downto 5) <= (others => '0');
				elsif address = "010000001001" then
					REGdata <= trig_divider;
				else
					REGdata <= (others => '0');
				end if;
			end if;
			
			if is_done = '1' then
				clr_done <= '1';
				trig_irq <= '1';
				trig_armed <= '0';
				trig_force <= '0';
			else
				clr_done <= '0';
			end if;

		end if;
	end process;
	------------------------------------------------------------------
	

	SAMPLING : component adcSampling
		port map (
			clk							=> clk,
			response_valid         	=> response_valid,         	
			response_channel       	=> response_channel,       	
			response_data          	=> response_data,          	
			response_startofpacket 	=> response_startofpacket, 	
			response_endofpacket   	=> response_endofpacket,
			command_channel 			=> command_channel,
			command_valid 				=> command_valid,
			command_startofpacket 	=> command_startofpacket,
			command_endofpacket 		=> command_endofpacket,
			command_ready 				=> command_ready,
			channel_CHA					=> channel_CHA,
			channel_CHB					=> channel_CHB,
			valid_data_CHA				=> valid_dataA,
			valid_data_CHB				=> valid_dataB
		);
		
	trigger1: trigger_hyst 
		port map(
			clk			=> clk,
			rst_n			=> reset_n,
			trigger_val	=> trigger_val,
			hyst_val		=> hyst_val,
			act_val(8 downto 0)		=> valid_dataT(11 downto 3),
			act_val(10 downto 9)		=> "00",
			trigger_r	=> trigger_intr,
			trigger_f	=> trigger_intf
		);
	
	trigger_module : trigger_simple
		port map(
			clk			=> clk,
			trigger		=> (trigger_int and trig_armed) or trig_force,
			is_idle		=> is_idle,
			is_done		=> is_done,
			clr_done		=> clr_done,
			divider		=> trig_divider,
			wraddress 	=> wraddress,
			wr_en			=> wr_en
		);
		
	with wraddress(0) select valid_data <=
		valid_dataA when '0',
		valid_dataB when '1';
	
	RAM_SEQ : SequencerRAM
		port map(
			data			=> valid_data(11 downto 3),
			rdaddress	=> address(9 downto 0),
			clock			=> clk,
			wraddress	=> wraddress,
			wren			=> wr_en,
			q				=> RAMdata(8 downto 0)
		);
	
END Structure;
