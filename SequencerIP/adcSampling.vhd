library ieee ;

use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity adcSampling is
	port(
		clk							: in  std_logic;
		response_valid				: in  std_logic;                                       
		response_channel			: in  std_logic_vector(4 downto 0);                    
		response_data				: in  std_logic_vector(11 downto 0);                   
		response_startofpacket	: in  std_logic;                                     
		response_endofpacket		: in  std_logic;
		command_channel 			: out std_logic_vector(4 downto 0);
		command_valid 				: out std_logic;
		command_startofpacket 	: out std_logic;
		command_endofpacket 		: out std_logic;
		command_ready 				: in std_logic;
		channel_CHA					: in std_logic_vector(4 downto 0);
		channel_CHB					: in std_logic_vector(4 downto 0);
		valid_data_CHA				: out std_logic_vector(11 downto 0);
		valid_data_CHB				: out std_logic_vector(11 downto 0)
	);
end adcSampling;


architecture basic of adcSampling is
	signal command_channel_local : std_logic_vector(4 downto 0) := "00000";
begin
	command_channel 			<= command_channel_local;
	command_valid 				<= '1';
	command_startofpacket 	<= '0';
	command_endofpacket 		<= '0';
	
	process (clk) is
	begin
		if rising_edge(clk) then
			--when channel is processed - change to next channel
			if command_ready = '1' then
				if command_channel_local = channel_CHA then
					command_channel_local <= channel_CHB;
				else
					command_channel_local <= channel_CHA;
				end if;
			end if;
			
			--when response is valid - latch it
			if response_valid = '1' then
				if response_channel = channel_CHA then
					valid_data_CHA <= response_data;
				end if;
				if response_channel = channel_CHB then
					valid_data_CHB <= response_data;
				end if;
			end if;
			
		end if;
	end process;
	
end basic;