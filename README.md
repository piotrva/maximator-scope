A simple oscilloscope based on MAX10 Intel FPGA chip 10M08DAC256C8G on maXimator board: http://maximator-fpga.org/

Description available at: ~~http://pak.info.pl/index.php?menu=artykulSzczegol&idArtykul=4718~~

As publiser website is down the article backup is available in this repo as [PAK_article.pdf](./PAK_article.pdf) or a [manuscript with color drawings](./PAK_article_manuscript_color.pdf)

Usage (simplified):
1. Upload *.sof (or *.pof for non-volatile configuration of FPGA) 
2. Connect VGA screen
3. Connect to pins B16, B15 and R15 buttons with external pull-up for menu (may use maXimator expander)
4. Connect analog signals (0-2.5V maximum rating) to pins F1 and E1

Screenshots of operating device:
![Mathematical operations](./Screenshots/MATH.png)
![Square waveform](./Screenshots/SQUARE.png)