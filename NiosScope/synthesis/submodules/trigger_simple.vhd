-- Project:			maximator-vga-oscilloscope
-- File:				trigger_simple.vhd
-- Version:			1.1 (24.04.2017)
-- Author:			Piotr Rzeszut (http://piotr94.net21.pl)
-- Description:	Processes trigger and controls display memory write

library ieee ;

use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity trigger_simple is
	port(
		clk					: in  std_logic;
		trigger				: in  std_logic;
		is_idle				: out std_logic;
		is_done				: out std_logic;
		clr_done				: in std_logic;
		divider				: in std_logic_vector(31 downto 0);
		wraddress 			: out std_logic_vector(9 downto 0);
		wr_en					: out std_logic
	);
end trigger_simple;

architecture basic of trigger_simple is
	signal prescaller			: std_logic_vector(31 downto 0);
	signal wraddress_int 	: std_logic_vector(9 downto 0);
	
	type trigger_state_t is (trigger_idle, trigger_copy, trigger_done);
	signal trigger_state : trigger_state_t;
begin

	wraddress <= wraddress_int;

	process(clk) is
	begin
		if falling_edge(clk) then
				case trigger_state is
					when trigger_idle =>
						is_done <= '0';
						is_idle <= '1';
						if trigger = '1' then
							trigger_state <= trigger_copy;
							wraddress_int <= (others => '0');
							wr_en <= '1';
						else
							trigger_state <= trigger_idle;
							wr_en <= '0';
						end if;
					when trigger_copy =>
						is_done <= '0';
						is_idle <= '0';
						if prescaller >= divider then
							prescaller <= (others => '0');
							if wraddress_int = "1111111111" then
								trigger_state <= trigger_done;
								wr_en <= '1';
							else
								trigger_state <= trigger_copy;
								wraddress_int <= wraddress_int + 1;
								wr_en <= '1';
							end if;
						else
							wr_en <= '0';
							prescaller <= prescaller +1;
						end if;
					when trigger_done =>
						wr_en <= '0';
						--wraddress_int <= (others => '0');
						is_done <= '1';
						is_idle <= '0';
						if clr_done = '1' then
							trigger_state <= trigger_idle;
						else
							trigger_state <= trigger_done;
						end if;
				end case;
		end if;
	end process;

end basic;