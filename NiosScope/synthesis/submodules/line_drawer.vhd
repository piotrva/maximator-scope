-- Project:			maximator-vga-oscilloscope
-- File:				line_drawer.vhd
-- Version:			1.1 (03.09.2016)
-- Author:			Piotr Rzeszut (http://piotr94.net21.pl)
-- Description:	Generates VGA data to draw waveform/line chart

library ieee ;

use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity line_drawer is 
	port(
		clk			: in	std_logic;
		rst_n			: in	std_logic;
		act_value	: in	std_logic_vector(10 downto 0);
		offset		: in	std_logic_vector(10 downto 0);
		bg_color		: in	std_logic_vector(2 downto 0);
		fg_color		: in	std_logic_vector(2 downto 0);
		px_addr		: in	std_logic_vector(10 downto 0);
		px_data		: out	std_logic_vector(2 downto 0);
		px_req		: out	std_logic
	);
end line_drawer;

architecture behav of line_drawer is
	signal y 		: std_logic_vector(10 downto 0);
	signal y_old	: std_logic_vector(10 downto 0);
begin
	
	process(clk, rst_n) begin
		if rst_n = '0' then
			y <= act_value;
			y_old <= act_value;
			px_data <= bg_color;
			px_req <= '0';
		elsif rising_edge(clk) then
			y_old <= y;
			y <= std_logic_vector(to_unsigned(512,11)) - act_value + offset;
			
			if (px_addr>=y and px_addr<=y_old) or (px_addr<=y and px_addr>=y_old) then
				px_data <= fg_color;
				px_req <= '1';
			else
				px_data <= bg_color;
				px_req <= '0';
			end if;
		end if;
	end process;

end behav;
