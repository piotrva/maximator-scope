-- Project:			maximator-vga-oscilloscope
-- File:				graph_drawer.vhd
-- Version:			1.0 (26.04.2017)
-- Author:			Piotr Rzeszut (http://piotr94.net21.pl)
-- Description:	Generates VGA data to draw waveform/line chart

library ieee ;

use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity graph_drawer is 
	port(
		clk			: in	std_logic;
		rst_n			: in	std_logic;
		graph_data	: in	std_logic_vector(15 downto 0);
		offset		: in	std_logic_vector(10 downto 0);
		bg_color		: in	std_logic_vector(2 downto 0);
		fg_color		: in	std_logic_vector(2 downto 0);
		px_addr		: in	std_logic_vector(10 downto 0);
		px_data		: out	std_logic_vector(2 downto 0);
		px_req		: out	std_logic
	);
end graph_drawer;

architecture behav of graph_drawer is
	--signal y 		: std_logic_vector(10 downto 0);
	--signal y_old	: std_logic_vector(10 downto 0);
	signal px_addr_int :	std_logic_vector(10 downto 0);
begin
	px_addr_int <= px_addr - offset;
	process(clk, rst_n) begin
		if rst_n = '0' then
			px_data <= bg_color;
			px_req <= '0';
		elsif rising_edge(clk) then
			if px_addr >= offset and px_addr <= (offset + std_logic_vector(to_unsigned(15,10))) then
				if graph_data(to_integer(unsigned(px_addr_int(3 downto 0)))) = '1' then
					px_data <= fg_color;
					px_req <= '1';
				else
					px_data <= bg_color;
					px_req <= '0';
				end if;
			else
				px_data <= bg_color;
				px_req <= '0';
			end if;
		end if;
	end process;

end behav;
