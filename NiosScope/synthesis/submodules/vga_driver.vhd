-- Project:			maximator-vga-oscilloscope
-- File:				vga_driver.vhd
-- Version:			1.1 (02.09.2016)
-- Author:			Piotr Rzeszut (http://piotr94.net21.pl)
-- Description:	Generates VGA signals and pixel address


library ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
--use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity vga_driver is
	generic(
		PX_WIDTH				: integer := 800;
		PX_FRONT_PORCH		: integer := 40;
		PX_SYNC_PULSE		: integer := 128;
		PX_BACK_PORCH		: integer := 88;
		LINE_HEIGHT			: integer := 600;
		LINE_FRONT_PORCH	: integer := 1;
		LINE_SYNC_PULSE	: integer := 4;
		LINE_BACK_PORCH	: integer := 23
	);
	port(
		vga_r			: out std_logic;
		vga_g			: out std_logic;
		vga_b			: out std_logic;
		vga_in		: in std_logic_vector(2 downto 0);
		vga_hs_n		: out std_logic;
		vga_vs_n		: out std_logic;
		px_addr_col	: out std_logic_vector(12 downto 0);
		px_addr_row	: out std_logic_vector(12 downto 0);
		clk			: in std_logic;
		rst_n			: in std_logic
	);
end vga_driver;

architecture basic of vga_driver is
	signal px_counter : integer := 0;
	signal line_counter : integer := 0;
	signal vga_r_int : std_logic;
	signal vga_g_int : std_logic;
	signal vga_b_int : std_logic;
	signal vga_blank_n : std_logic;
	signal vga_vs_n_int : std_logic;
	signal vga_hs_n_int : std_logic;
	signal px_addr_int : integer := 0;
begin
	
	vga_r <= vga_r_int and (vga_vs_n_int and vga_hs_n_int and vga_blank_n);
	vga_g <= vga_g_int and (vga_vs_n_int and vga_hs_n_int and vga_blank_n);
	vga_b <= vga_b_int and (vga_vs_n_int and vga_hs_n_int and vga_blank_n);
	
	vga_hs_n <= vga_hs_n_int;
	vga_vs_n <= vga_vs_n_int;
	
	--px_addr <= px_addr_int;
	--px_addr_col <= px_addr_int mod ;
	--px_addr_row <= std_logic_vector(to_unsigned(line_counter, px_addr_row'length));
	
	px_addr_col <= std_logic_vector(to_unsigned((px_addr_int mod PX_WIDTH), px_addr_col'length));
	px_addr_row <= std_logic_vector(to_unsigned((px_addr_int / PX_WIDTH), px_addr_row'length));
	
	
	process(clk, rst_n)
	begin
		if rst_n='0' then
			px_counter <= 0;
			line_counter <= 0;
			px_addr_int <= 0;
		elsif falling_edge(clk) then
			
			if px_counter<PX_WIDTH then--data
				if line_counter<LINE_HEIGHT then
					vga_r_int <= vga_in(0);
					vga_g_int <= vga_in(1);
					vga_b_int <= vga_in(2);
					px_addr_int <= px_addr_int + 1;
					
					vga_blank_n <= '1';
					vga_hs_n_int <= '1';
				else
					vga_blank_n <= '0';
					vga_hs_n_int <= '1';
				end if;
				px_counter <= px_counter + 1;
			elsif px_counter<(PX_WIDTH+PX_FRONT_PORCH) then--front porch
				vga_blank_n <= '0';
				px_counter <= px_counter + 1;
			elsif px_counter<(PX_WIDTH+PX_FRONT_PORCH+PX_SYNC_PULSE) then--HS
				vga_blank_n <= '0';
				vga_hs_n_int <= '0';
				px_counter <= px_counter + 1;
			elsif px_counter<(PX_WIDTH+PX_FRONT_PORCH+PX_SYNC_PULSE+PX_BACK_PORCH-1) then--back porch
				vga_blank_n <= '0';
				vga_hs_n_int <= '1';
				px_counter <= px_counter + 1;
			else
				px_counter <= 0;
				if line_counter<LINE_HEIGHT then--video
					line_counter <= line_counter + 1;
					vga_vs_n_int <= '1';
				elsif line_counter<(LINE_HEIGHT+LINE_FRONT_PORCH) then--front porch
					line_counter <= line_counter + 1;
				elsif line_counter<(LINE_HEIGHT+LINE_FRONT_PORCH+LINE_SYNC_PULSE) then--vs
					line_counter <= line_counter + 1;
					vga_vs_n_int <= '0';
				elsif line_counter<(LINE_HEIGHT+LINE_FRONT_PORCH+LINE_SYNC_PULSE+LINE_BACK_PORCH-1) then--back porch
					line_counter <= line_counter + 1;
					vga_vs_n_int <= '1';
				else
					line_counter <= 0;
					px_addr_int <= 0;
				end if;
			end if;
			
			
		end if;
	end process;
end basic;