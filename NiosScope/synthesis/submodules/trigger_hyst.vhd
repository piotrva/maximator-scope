-- Project:			maximator-vga-oscilloscope
-- File:				trigger.vhd
-- Version:			1.1 (24.04.2017)
-- Author:			Piotr Rzeszut (http://piotr94.net21.pl)
-- Description:	Triggers sampling

library ieee ;

use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity trigger_hyst is 
	port(
		clk			: in	std_logic;
		rst_n			: in	std_logic;
		trigger_val	: in	std_logic_vector(10 downto 0);
		hyst_val		: in	std_logic_vector(10 downto 0);
		act_val		: in	std_logic_vector(10 downto 0);
		trigger_r	: out	std_logic;
		trigger_f	: out	std_logic
	);
end trigger_hyst;

architecture behav of trigger_hyst is
	signal state 				: std_logic;
	signal reported 			: std_logic;
begin

	trigger_r <= ((state xor reported) and state);
	trigger_f <= ((state xor reported) and (not state));
	
	process(clk, rst_n) begin
		if rst_n = '0' then
			state <= '1';
			reported <= '1';
		elsif falling_edge(clk) then
			if state = '1' then
				if act_val < (trigger_val - hyst_val) then
					state <= '0';
					reported <= '1';
				else
					state <= '1';
					reported <= '1';
				end if;
			else
				if act_val > (trigger_val + hyst_val) then
					state <= '1';
					reported <= '0';
				else
					state <= '0';
					reported <= '0';
				end if;
			end if;
		end if;
	end process;

end behav;
