-- Project:			maximator-vga-oscilloscope
-- File:				grid_drawer.vhd
-- Version:			1.1 (03.09.2016)
-- Author:			Piotr Rzeszut (http://piotr94.net21.pl)
-- Description:	Generates VGA data to draw trigger line

library ieee ;

use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity grid_drawer is 
	port(
		clk			: in	std_logic;
		rst_n			: in	std_logic;
		offset		: in	std_logic_vector(10 downto 0);
		bg_color		: in	std_logic_vector(2 downto 0);
		fg_color		: in	std_logic_vector(2 downto 0);
		px_addr_row	: in	std_logic_vector(10 downto 0);
		px_addr_col	: in	std_logic_vector(10 downto 0);
		px_data		: out	std_logic_vector(2 downto 0);
		px_req		: out	std_logic
	);
end grid_drawer;

architecture behav of grid_drawer is
	signal px_addr_row_loc : std_logic_vector(10 downto 0);
begin
	
	px_addr_row_loc <= px_addr_row - offset;
	
	process(clk, rst_n) begin
		if rst_n = '0' then
			px_data <= bg_color;
			px_req <= '0';
		elsif rising_edge(clk) then
			if px_addr_row >= offset and px_addr_row <= (offset + std_logic_vector(to_unsigned(512,11))) then
				if px_addr_row_loc(5 downto 0) = "111111" or px_addr_row_loc = "00000000000" then
					px_data <= fg_color;
					px_req <= '1';
				elsif px_addr_col(5 downto 0) = "111111" or px_addr_col = "00000000000" then
					px_data <= fg_color;
					px_req <= '1';
				else
					px_data <= bg_color;
					px_req <= '0';
				end if;
			else
				px_data <= bg_color;
				px_req <= '0';
			end if;
		end if;
	end process;

end behav;
