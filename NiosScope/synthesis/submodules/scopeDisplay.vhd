-- Project:			maximator-NiosScope
-- File:				scopeDisplay.vhd
-- Version:			1.0 (14.04.2017)
-- Author:			Piotr Rzeszut (http://piotr94.net21.pl)
-- Description:	Generates VGA picture based on ram contents that may be written through Avalon-MM

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;


ENTITY scopeDisplay IS
	PORT (	
				--avalon Memory-Mapped slave
				clk			: IN STD_LOGIC;
				reset_n		: IN STD_LOGIC;
				address		: IN STD_LOGIC_VECTOR(12 DOWNTO 0);
				byteenable	: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
				read			: IN STD_LOGIC;
				readdata		: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
				write			: IN STD_LOGIC;
				writedata	: IN STD_LOGIC_VECTOR(31 DOWNTO 0);
				--chipselect: IN STD_LOGIC;
				
				--VGA exported interface
				VGA_CLK		: IN STD_LOGIC;
				VGA_R			: OUT STD_LOGIC;
				VGA_G			: OUT STD_LOGIC;
				VGA_B			: OUT STD_LOGIC;
				VGA_HS		: OUT STD_LOGIC;
				VGA_VS		: OUT STD_LOGIC
		);
END scopeDisplay;


ARCHITECTURE Structure OF scopeDisplay IS

	component displayRAM
		PORT
		(
			data		: IN STD_LOGIC_VECTOR (8 DOWNTO 0);
			rdaddress		: IN STD_LOGIC_VECTOR (9 DOWNTO 0);
			rdclock		: IN STD_LOGIC ;
			wraddress		: IN STD_LOGIC_VECTOR (9 DOWNTO 0);
			wrclock		: IN STD_LOGIC  := '1';
			wren		: IN STD_LOGIC  := '0';
			q		: OUT STD_LOGIC_VECTOR (8 DOWNTO 0)
		);
	end component;
	
	component graphRAM
		PORT
		(
			data		: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
			rdaddress		: IN STD_LOGIC_VECTOR (9 DOWNTO 0);
			rdclock		: IN STD_LOGIC ;
			wraddress		: IN STD_LOGIC_VECTOR (9 DOWNTO 0);
			wrclock		: IN STD_LOGIC  := '1';
			wren		: IN STD_LOGIC  := '0';
			q		: OUT STD_LOGIC_VECTOR (15 DOWNTO 0)
		);
	end component;
	
	component vga_driver is
		generic(
			PX_WIDTH				: integer := 800;
			PX_FRONT_PORCH		: integer := 40;
			PX_SYNC_PULSE		: integer := 128;
			PX_BACK_PORCH		: integer := 88;
			LINE_HEIGHT			: integer := 600;
			LINE_FRONT_PORCH	: integer := 1;
			LINE_SYNC_PULSE	: integer := 4;
			LINE_BACK_PORCH	: integer := 23
		);
		port(
			vga_r			: out std_logic;
			vga_g			: out std_logic;
			vga_b			: out std_logic;
			vga_in		: in std_logic_vector(2 downto 0);
			vga_hs_n		: out std_logic;
			vga_vs_n		: out std_logic;
			px_addr_col	: out std_logic_vector(12 downto 0);
			px_addr_row	: out std_logic_vector(12 downto 0);
			clk		: in std_logic;
			rst_n			: in std_logic
		);
	end component vga_driver;
	
	component line_drawer is 
		port(
			clk			: in	std_logic;
			rst_n			: in	std_logic;
			act_value	: in	std_logic_vector(10 downto 0);
			offset		: in	std_logic_vector(10 downto 0);
			bg_color		: in	std_logic_vector(2 downto 0);
			fg_color	: in	std_logic_vector(2 downto 0);
			px_addr		: in	std_logic_vector(10 downto 0);
			px_data		: out	std_logic_vector(2 downto 0);
			px_req		: out	std_logic
		);
	end component line_drawer;
	
	component graph_drawer is 
		port(
			clk			: in	std_logic;
			rst_n			: in	std_logic;
			graph_data	: in	std_logic_vector(15 downto 0);
			offset		: in	std_logic_vector(10 downto 0);
			bg_color		: in	std_logic_vector(2 downto 0);
			fg_color		: in	std_logic_vector(2 downto 0);
			px_addr		: in	std_logic_vector(10 downto 0);
			px_data		: out	std_logic_vector(2 downto 0);
			px_req		: out	std_logic
		);
	end component graph_drawer;
	
	component trigger_drawer is 
		port(
			clk			: in	std_logic;
			rst_n			: in	std_logic;
			trigger_val	: in	std_logic_vector(10 downto 0);
			offset		: in	std_logic_vector(10 downto 0);
			bg_color		: in	std_logic_vector(2 downto 0);
			fg_color		: in	std_logic_vector(2 downto 0);
			px_addr		: in	std_logic_vector(10 downto 0);
			px_data		: out	std_logic_vector(2 downto 0);
			px_req		: out	std_logic
		);
	end component trigger_drawer;
	
	component grid_drawer is 
		port(
			clk			: in	std_logic;
			rst_n			: in	std_logic;
			offset		: in	std_logic_vector(10 downto 0);
			bg_color		: in	std_logic_vector(2 downto 0);
			fg_color		: in	std_logic_vector(2 downto 0);
			px_addr_row	: in	std_logic_vector(10 downto 0);
			px_addr_col	: in	std_logic_vector(10 downto 0);
			px_data		: out	std_logic_vector(2 downto 0);
			px_req		: out	std_logic
		);
	end component grid_drawer;
	
	component priority_mux is 
		port(
			bg_color		: in	std_logic_vector(2 downto 0);
			px_data1		: in	std_logic_vector(2 downto 0);
			px_req1		: in	std_logic;
			px_data2		: in	std_logic_vector(2 downto 0);
			px_req2		: in	std_logic;
			px_data3		: in	std_logic_vector(2 downto 0);
			px_req3		: in	std_logic;
			px_data4		: in	std_logic_vector(2 downto 0);
			px_req4		: in	std_logic;
			px_data5		: in	std_logic_vector(2 downto 0);
			px_req5		: in	std_logic;
			px_data6		: in	std_logic_vector(2 downto 0);
			px_req6		: in	std_logic;
			px_data		: out	std_logic_vector(2 downto 0)
		);
	end component priority_mux;
	
	
	signal px_data 	:std_logic_vector(2 downto 0);
	signal px_data1 	:std_logic_vector(2 downto 0);
	signal px_data2 	:std_logic_vector(2 downto 0);
	signal px_data3 	:std_logic_vector(2 downto 0);
	signal px_data4 	:std_logic_vector(2 downto 0);
	signal px_data5 	:std_logic_vector(2 downto 0);
	signal px_data6 	:std_logic_vector(2 downto 0);
	
	signal px_req1		:std_logic;
	signal px_req2		:std_logic;
	signal px_req3		:std_logic;
	signal px_req4		:std_logic;
	signal px_req5		:std_logic;
	signal px_req6		:std_logic;
	
	signal px_addr_col:std_logic_vector(12 downto 0);
	signal px_addr_row:std_logic_vector(12 downto 0);
	
	signal act_valueA	:std_logic_vector(10 downto 0);
	signal act_valueB	:std_logic_vector(10 downto 0);
	signal act_valueC	:std_logic_vector(10 downto 0);
	
	signal graphData	:std_logic_vector(15 downto 0);
	
	signal writeA		:std_logic;
	signal writeB		:std_logic;
	signal writeC		:std_logic;
	signal writeG		:std_logic;
	
	signal selA			:std_logic;
	signal selB			:std_logic;
	signal selC			:std_logic;
	signal selG			:std_logic;
	
	signal offset				:std_logic_vector(10 downto 0);
	signal trigger_val		:std_logic_vector(10 downto 0);
	signal text_offset		:std_logic_vector(10 downto 0);
	signal enable_elements	:std_logic_vector(5 downto 0);
	signal bg_color			:std_logic_vector(2 downto 0);
	signal linea_color		:std_logic_vector(2 downto 0);
	signal lineb_color		:std_logic_vector(2 downto 0);
	signal linec_color		:std_logic_vector(2 downto 0);
	signal trig_color			:std_logic_vector(2 downto 0);
	signal grid_color			:std_logic_vector(2 downto 0);
	signal text_color			:std_logic_vector(2 downto 0);
	
	
BEGIN	
	
	selA <= 	'1' when address(12 downto 10) = "000" else
					'0';
	selB <= 	'1' when address(12 downto 10) = "001" else
					'0';
	selC <= 	'1' when address(12 downto 10) = "010" else
					'0';
	selG <= 	'1' when address(12 downto 10) = "011" else
					'0';
					
	writeA <= selA and write;
	writeB <= selB and write;
	writeC <= selC and write;
	writeG <= selG and write;
	
	process(clk, reset_n) is
	begin
		if reset_n ='0' then
			offset 				<= std_logic_vector(to_unsigned(128,offset'length));
			trigger_val 		<= std_logic_vector(to_unsigned(256,trigger_val'length));
			text_offset			<= std_logic_vector(to_unsigned(50,text_offset'length));
			enable_elements	<= "111111";
			bg_color				<= "000";
			linea_color			<= "111";
			lineb_color			<= "110";
			linec_color			<= "011";
			trig_color			<= "010";
			grid_color			<= "001";
			text_color			<= "111";
		elsif rising_edge(clk) then
			if write = '1' then
				if address = "1000000000000" then
					offset <= writedata(10 downto 0);
				elsif address = "1000000000001" then
					trigger_val <= writedata(10 downto 0);
				elsif address = "1000000000010" then
					text_offset <= writedata(10 downto 0);
				elsif address = "1000000000011" then
					enable_elements <= writedata(5 downto 0);
				elsif address = "1000000000100" then
					bg_color <= writedata(2 downto 0);
				elsif address = "1000000000101" then
					linea_color <= writedata(2 downto 0);
				elsif address = "1000000000110" then
					lineb_color <= writedata(2 downto 0);
				elsif address = "1000000000111" then
					linec_color <= writedata(2 downto 0);
				elsif address = "1000000001000" then
					trig_color <= writedata(2 downto 0);
				elsif address = "1000000001001" then
					grid_color <= writedata(2 downto 0);
				elsif address = "1000000001010" then
					text_color <= writedata(2 downto 0);
				end if;
			elsif read = '1' then
				if address = "1000000000000" then
					readdata(10 downto 0) <= offset;
					readdata(31 downto 11) <= (others => '0');
				elsif address = "1000000000001" then
					readdata(10 downto 0) <= trigger_val;
					readdata(31 downto 11) <= (others => '0');
				elsif address = "1000000000010" then
					readdata(10 downto 0) <= text_offset;
					readdata(31 downto 11) <= (others => '0');
				elsif address = "1000000000011" then
					readdata(5 downto 0) <= enable_elements;
					readdata(31 downto 6) <= (others => '0');
				elsif address = "1000000000100" then
					readdata(2 downto 0) <= bg_color;
					readdata(31 downto 3) <= (others => '0');
				elsif address = "1000000000101" then
					readdata(2 downto 0) <= linea_color;
					readdata(31 downto 3) <= (others => '0');
				elsif address = "1000000000110" then
					readdata(2 downto 0) <= lineb_color;
					readdata(31 downto 3) <= (others => '0');
				elsif address = "1000000000111" then
					readdata(2 downto 0) <= linec_color;
					readdata(31 downto 3) <= (others => '0');
				elsif address = "1000000001000" then
					readdata(2 downto 0) <= trig_color;
					readdata(31 downto 3) <= (others => '0');
				elsif address = "1000000001001" then
					readdata(2 downto 0) <= grid_color;
					readdata(31 downto 3) <= (others => '0');
				elsif address = "1000000001010" then
					readdata(2 downto 0) <= text_color;
					readdata(31 downto 3) <= (others => '0');
				else
					readdata <= (others => '0');
				end if;
			end if;
		end if;
	end process;
	
	VGA1 : vga_driver
		generic map(
			PX_WIDTH				=> 1024,
			PX_FRONT_PORCH		=> 24,
			PX_SYNC_PULSE		=> 136,
			PX_BACK_PORCH		=> 160,
			LINE_HEIGHT			=> 768,
			LINE_FRONT_PORCH	=> 3,
			LINE_SYNC_PULSE	=> 6,
			LINE_BACK_PORCH	=> 29
		)
		port map(
			vga_r			=> vga_r,
			vga_g			=> vga_g,
			vga_b			=> vga_b,
			vga_in		=> px_data,
			vga_hs_n		=> vga_hs,
			vga_vs_n		=> vga_vs,
			px_addr_col	=> px_addr_col,
			px_addr_row	=> px_addr_row,
			clk			=> vga_clk,
			rst_n			=> reset_n
		);
		
	LINEA : line_drawer
		port map(
			clk			=> vga_clk,
			rst_n			=> reset_n,
			act_value	=> act_valueA,
			offset		=> offset,
			bg_color		=> bg_color,
			fg_color		=> linea_color,
			px_addr		=> px_addr_row(10 downto 0),
			px_data		=> px_data1,
			px_req		=> px_req1
		);
		
	LINEB : line_drawer
		port map(
			clk			=> vga_clk,
			rst_n			=> reset_n,
			act_value	=> act_valueB,
			offset		=> offset,
			bg_color		=> bg_color,
			fg_color		=> lineb_color,
			px_addr		=> px_addr_row(10 downto 0),
			px_data		=> px_data3,
			px_req		=> px_req3
		);
		
	LINEC : line_drawer
		port map(
			clk			=> vga_clk,
			rst_n			=> reset_n,
			act_value	=> act_valueC,
			offset		=> offset,
			bg_color		=> bg_color,
			fg_color		=> linec_color,
			px_addr		=> px_addr_row(10 downto 0),
			px_data		=> px_data4,
			px_req		=> px_req4
		);
	
	GRAPH : graph_drawer
		port map(
			clk			=> vga_clk,
			rst_n			=> reset_n,
			graph_data	=> graphData,
			offset		=> text_offset,
			bg_color		=> bg_color,
			fg_color		=> text_color,
			px_addr		=> px_addr_row(10 downto 0),
			px_data		=> px_data5,
			px_req		=> px_req5
		);
		
	TRIGGER_DARW1 : trigger_drawer
		port map(
			clk			=> vga_clk,
			rst_n			=> reset_n,
			trigger_val	=> trigger_val,
			offset		=> offset,
			bg_color		=> bg_color,
			fg_color		=> trig_color,
			px_addr		=> px_addr_row(10 downto 0),
			px_data		=> px_data2,
			px_req		=> px_req2
		);
		
	GRID_DRAW: grid_drawer
		port map(
			clk			=> vga_clk,
			rst_n			=> reset_n,
			offset		=> offset,
			bg_color		=> bg_color,
			fg_color		=> grid_color,
			px_addr_row	=> px_addr_row(10 downto 0),
			px_addr_col	=> px_addr_col(10 downto 0),
			px_data		=> px_data6,
			px_req		=> px_req6
		);
	
	RAM_DISPA : displayRAM
		port map(
			data			=> writedata(8 downto 0),
			rdaddress	=> px_addr_col(9 downto 0),
			rdclock		=> vga_clk,
			wraddress	=> address(9 downto 0),
			wrclock		=> clk,
			wren			=> writeA,
			q				=> act_valueA(8 downto 0)
		);
	
	RAM_DISPB : displayRAM
		port map(
			data			=> writedata(8 downto 0),
			rdaddress	=> px_addr_col(9 downto 0),
			rdclock		=> vga_clk,
			wraddress	=> address(9 downto 0),
			wrclock		=> clk,
			wren			=> writeB,
			q				=> act_valueB(8 downto 0)
		);
	
	RAM_DISPC : displayRAM
		port map(
			data			=> writedata(8 downto 0),
			rdaddress	=> px_addr_col(9 downto 0),
			rdclock		=> vga_clk,
			wraddress	=> address(9 downto 0),
			wrclock		=> clk,
			wren			=> writeC,
			q				=> act_valueC(8 downto 0)
		);
	
	RAM_DISPG : graphRAM 
		port map(
			data	 		=> writedata(15 downto 0),
			rdaddress	=> px_addr_col(9 downto 0),
			rdclock	 	=> vga_clk,
			wraddress	=> address(9 downto 0),
			wrclock	 	=> clk,
			wren	 		=> writeG,
			q	 			=> graphData
		);
		
	MUX1 : priority_mux
		port map(
			bg_color		=> bg_color,
			px_data1		=> px_data1,
			px_req1		=> (px_req1 and enable_elements(0)),
			px_data2		=> px_data2,
			px_req2		=> (px_req2 and enable_elements(1)),
			px_data3		=> px_data3,
			px_req3		=> (px_req3 and enable_elements(2)),
			px_data4		=> px_data4,
			px_req4		=> (px_req4 and enable_elements(3)),
			px_data5		=> px_data5,
			px_req5		=> (px_req5 and enable_elements(4)),
			px_data6		=> px_data6,
			px_req6		=> (px_req6 and enable_elements(5)),
			px_data		=> px_data
		);
	
END Structure;
