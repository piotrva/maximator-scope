	component NiosScope is
		port (
			btn_export    : in  std_logic_vector(2 downto 0) := (others => 'X'); -- export
			clk_clk       : in  std_logic                    := 'X';             -- clk
			led_export    : out std_logic_vector(3 downto 0);                    -- export
			reset_reset_n : in  std_logic                    := 'X';             -- reset_n
			uart_rxd      : in  std_logic                    := 'X';             -- rxd
			uart_txd      : out std_logic;                                       -- txd
			vga_b_out     : out std_logic;                                       -- out
			vga_g_out     : out std_logic;                                       -- out
			vga_hs_out    : out std_logic;                                       -- out
			vga_r_out     : out std_logic;                                       -- out
			vga_vs_out    : out std_logic                                        -- out
		);
	end component NiosScope;

	u0 : component NiosScope
		port map (
			btn_export    => CONNECTED_TO_btn_export,    --    btn.export
			clk_clk       => CONNECTED_TO_clk_clk,       --    clk.clk
			led_export    => CONNECTED_TO_led_export,    --    led.export
			reset_reset_n => CONNECTED_TO_reset_reset_n, --  reset.reset_n
			uart_rxd      => CONNECTED_TO_uart_rxd,      --   uart.rxd
			uart_txd      => CONNECTED_TO_uart_txd,      --       .txd
			vga_b_out     => CONNECTED_TO_vga_b_out,     --  vga_b.out
			vga_g_out     => CONNECTED_TO_vga_g_out,     --  vga_g.out
			vga_hs_out    => CONNECTED_TO_vga_hs_out,    -- vga_hs.out
			vga_r_out     => CONNECTED_TO_vga_r_out,     --  vga_r.out
			vga_vs_out    => CONNECTED_TO_vga_vs_out     -- vga_vs.out
		);

