
module NiosScope (
	btn_export,
	clk_clk,
	led_export,
	reset_reset_n,
	uart_rxd,
	uart_txd,
	vga_b_out,
	vga_g_out,
	vga_hs_out,
	vga_r_out,
	vga_vs_out);	

	input	[2:0]	btn_export;
	input		clk_clk;
	output	[3:0]	led_export;
	input		reset_reset_n;
	input		uart_rxd;
	output		uart_txd;
	output		vga_b_out;
	output		vga_g_out;
	output		vga_hs_out;
	output		vga_r_out;
	output		vga_vs_out;
endmodule
