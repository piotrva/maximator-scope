	NiosScope u0 (
		.btn_export    (<connected-to-btn_export>),    //    btn.export
		.clk_clk       (<connected-to-clk_clk>),       //    clk.clk
		.led_export    (<connected-to-led_export>),    //    led.export
		.reset_reset_n (<connected-to-reset_reset_n>), //  reset.reset_n
		.uart_rxd      (<connected-to-uart_rxd>),      //   uart.rxd
		.uart_txd      (<connected-to-uart_txd>),      //       .txd
		.vga_b_out     (<connected-to-vga_b_out>),     //  vga_b.out
		.vga_g_out     (<connected-to-vga_g_out>),     //  vga_g.out
		.vga_hs_out    (<connected-to-vga_hs_out>),    // vga_hs.out
		.vga_r_out     (<connected-to-vga_r_out>),     //  vga_r.out
		.vga_vs_out    (<connected-to-vga_vs_out>)     // vga_vs.out
	);

