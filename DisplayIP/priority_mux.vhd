-- Project:			maximator-NiosScope
-- File:				priority_mux.vhd
-- Version:			1.1 (15.04.2017)
-- Author:			Piotr Rzeszut (http://piotr94.net21.pl)
-- Description:	Selects module to have access to VGA output

library ieee ;

use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity priority_mux is 
	port(
		bg_color		: in	std_logic_vector(2 downto 0);
		px_data1		: in	std_logic_vector(2 downto 0);
		px_req1		: in	std_logic;
		px_data2		: in	std_logic_vector(2 downto 0);
		px_req2		: in	std_logic;
		px_data3		: in	std_logic_vector(2 downto 0);
		px_req3		: in	std_logic;
		px_data4		: in	std_logic_vector(2 downto 0);
		px_req4		: in	std_logic;
		px_data5		: in	std_logic_vector(2 downto 0);
		px_req5		: in	std_logic;
		px_data6		: in	std_logic_vector(2 downto 0);
		px_req6		: in	std_logic;
		px_data		: out	std_logic_vector(2 downto 0)
	);
end priority_mux;

architecture behav of priority_mux is
begin
	
	px_data <= 		px_data1 when px_req1 = '1' else
						px_data2 when px_req2 = '1' else
						px_data3 when px_req3 = '1' else
						px_data4 when px_req4 = '1' else
						px_data5 when px_req5 = '1' else
						px_data6 when px_req6 = '1' else
						bg_color;

end behav;
