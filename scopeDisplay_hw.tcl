# TCL File Generated by Component Editor 16.1
# Sun Apr 30 22:40:49 CEST 2017
# DO NOT MODIFY


# 
# scopeDisplay "scopeDisplay" v1.0
# Piotr Rzeszut 2017.04.30.22:40:49
# 
# 

# 
# request TCL package from ACDS 16.1
# 
package require -exact qsys 16.1


# 
# module scopeDisplay
# 
set_module_property DESCRIPTION ""
set_module_property NAME scopeDisplay
set_module_property VERSION 1.0
set_module_property INTERNAL false
set_module_property OPAQUE_ADDRESS_MAP true
set_module_property GROUP "NiosScope IP Cores"
set_module_property AUTHOR "Piotr Rzeszut"
set_module_property DISPLAY_NAME scopeDisplay
set_module_property INSTANTIATE_IN_SYSTEM_MODULE true
set_module_property EDITABLE true
set_module_property REPORT_TO_TALKBACK false
set_module_property ALLOW_GREYBOX_GENERATION false
set_module_property REPORT_HIERARCHY false


# 
# file sets
# 
add_fileset QUARTUS_SYNTH QUARTUS_SYNTH "" ""
set_fileset_property QUARTUS_SYNTH TOP_LEVEL scopeDisplay
set_fileset_property QUARTUS_SYNTH ENABLE_RELATIVE_INCLUDE_PATHS false
set_fileset_property QUARTUS_SYNTH ENABLE_FILE_OVERWRITE_MODE false
add_fileset_file scopeDisplay.vhd VHDL PATH DisplayIP/scopeDisplay.vhd TOP_LEVEL_FILE
add_fileset_file priority_mux.vhd VHDL PATH DisplayIP/priority_mux.vhd
add_fileset_file line_drawer.vhd VHDL PATH DisplayIP/vga_drivers/line_drawer.vhd
add_fileset_file trigger_drawer.vhd VHDL PATH DisplayIP/vga_drivers/trigger_drawer.vhd
add_fileset_file vga_driver.vhd VHDL PATH DisplayIP/vga_drivers/vga_driver.vhd
add_fileset_file graph_drawer.vhd VHDL PATH DisplayIP/vga_drivers/graph_drawer.vhd
add_fileset_file grid_drawer.vhd VHDL PATH DisplayIP/vga_drivers/grid_drawer.vhd

add_fileset SIM_VHDL SIM_VHDL "" ""
set_fileset_property SIM_VHDL TOP_LEVEL scopeDisplay
set_fileset_property SIM_VHDL ENABLE_RELATIVE_INCLUDE_PATHS false
set_fileset_property SIM_VHDL ENABLE_FILE_OVERWRITE_MODE false
add_fileset_file scopeDisplay.vhd VHDL PATH DisplayIP/scopeDisplay.vhd


# 
# parameters
# 


# 
# display items
# 


# 
# connection point clock
# 
add_interface clock clock end
set_interface_property clock clockRate 0
set_interface_property clock ENABLED true
set_interface_property clock EXPORT_OF ""
set_interface_property clock PORT_NAME_MAP ""
set_interface_property clock CMSIS_SVD_VARIABLES ""
set_interface_property clock SVD_ADDRESS_GROUP ""

add_interface_port clock clk clk Input 1


# 
# connection point reset
# 
add_interface reset reset end
set_interface_property reset associatedClock clock
set_interface_property reset synchronousEdges DEASSERT
set_interface_property reset ENABLED true
set_interface_property reset EXPORT_OF ""
set_interface_property reset PORT_NAME_MAP ""
set_interface_property reset CMSIS_SVD_VARIABLES ""
set_interface_property reset SVD_ADDRESS_GROUP ""

add_interface_port reset reset_n reset_n Input 1


# 
# connection point avalon_slave_0
# 
add_interface avalon_slave_0 avalon end
set_interface_property avalon_slave_0 addressUnits WORDS
set_interface_property avalon_slave_0 associatedClock clock
set_interface_property avalon_slave_0 associatedReset reset
set_interface_property avalon_slave_0 bitsPerSymbol 8
set_interface_property avalon_slave_0 burstOnBurstBoundariesOnly false
set_interface_property avalon_slave_0 burstcountUnits WORDS
set_interface_property avalon_slave_0 explicitAddressSpan 0
set_interface_property avalon_slave_0 holdTime 0
set_interface_property avalon_slave_0 linewrapBursts false
set_interface_property avalon_slave_0 maximumPendingReadTransactions 0
set_interface_property avalon_slave_0 maximumPendingWriteTransactions 0
set_interface_property avalon_slave_0 readLatency 0
set_interface_property avalon_slave_0 readWaitTime 1
set_interface_property avalon_slave_0 setupTime 0
set_interface_property avalon_slave_0 timingUnits Cycles
set_interface_property avalon_slave_0 writeWaitTime 0
set_interface_property avalon_slave_0 ENABLED true
set_interface_property avalon_slave_0 EXPORT_OF ""
set_interface_property avalon_slave_0 PORT_NAME_MAP ""
set_interface_property avalon_slave_0 CMSIS_SVD_VARIABLES ""
set_interface_property avalon_slave_0 SVD_ADDRESS_GROUP ""

add_interface_port avalon_slave_0 address address Input 13
add_interface_port avalon_slave_0 byteenable byteenable Input 4
add_interface_port avalon_slave_0 read read Input 1
add_interface_port avalon_slave_0 readdata readdata Output 32
add_interface_port avalon_slave_0 write write Input 1
add_interface_port avalon_slave_0 writedata writedata Input 32
set_interface_assignment avalon_slave_0 embeddedsw.configuration.isFlash 0
set_interface_assignment avalon_slave_0 embeddedsw.configuration.isMemoryDevice 0
set_interface_assignment avalon_slave_0 embeddedsw.configuration.isNonVolatileStorage 0
set_interface_assignment avalon_slave_0 embeddedsw.configuration.isPrintableDevice 0


# 
# connection point vga_clock
# 
add_interface vga_clock clock end
set_interface_property vga_clock clockRate 0
set_interface_property vga_clock ENABLED true
set_interface_property vga_clock EXPORT_OF ""
set_interface_property vga_clock PORT_NAME_MAP ""
set_interface_property vga_clock CMSIS_SVD_VARIABLES ""
set_interface_property vga_clock SVD_ADDRESS_GROUP ""

add_interface_port vga_clock VGA_CLK clk Input 1


# 
# connection point VGA_R
# 
add_interface VGA_R conduit end
set_interface_property VGA_R associatedClock vga_clock
set_interface_property VGA_R associatedReset ""
set_interface_property VGA_R ENABLED true
set_interface_property VGA_R EXPORT_OF ""
set_interface_property VGA_R PORT_NAME_MAP ""
set_interface_property VGA_R CMSIS_SVD_VARIABLES ""
set_interface_property VGA_R SVD_ADDRESS_GROUP ""

add_interface_port VGA_R VGA_R out Output 1


# 
# connection point VGA_G
# 
add_interface VGA_G conduit end
set_interface_property VGA_G associatedClock vga_clock
set_interface_property VGA_G associatedReset ""
set_interface_property VGA_G ENABLED true
set_interface_property VGA_G EXPORT_OF ""
set_interface_property VGA_G PORT_NAME_MAP ""
set_interface_property VGA_G CMSIS_SVD_VARIABLES ""
set_interface_property VGA_G SVD_ADDRESS_GROUP ""

add_interface_port VGA_G VGA_G out Output 1


# 
# connection point VGA_B
# 
add_interface VGA_B conduit end
set_interface_property VGA_B associatedClock vga_clock
set_interface_property VGA_B associatedReset ""
set_interface_property VGA_B ENABLED true
set_interface_property VGA_B EXPORT_OF ""
set_interface_property VGA_B PORT_NAME_MAP ""
set_interface_property VGA_B CMSIS_SVD_VARIABLES ""
set_interface_property VGA_B SVD_ADDRESS_GROUP ""

add_interface_port VGA_B VGA_B out Output 1


# 
# connection point VGA_HS
# 
add_interface VGA_HS conduit end
set_interface_property VGA_HS associatedClock vga_clock
set_interface_property VGA_HS associatedReset ""
set_interface_property VGA_HS ENABLED true
set_interface_property VGA_HS EXPORT_OF ""
set_interface_property VGA_HS PORT_NAME_MAP ""
set_interface_property VGA_HS CMSIS_SVD_VARIABLES ""
set_interface_property VGA_HS SVD_ADDRESS_GROUP ""

add_interface_port VGA_HS VGA_HS out Output 1


# 
# connection point VGA_VS
# 
add_interface VGA_VS conduit end
set_interface_property VGA_VS associatedClock vga_clock
set_interface_property VGA_VS associatedReset ""
set_interface_property VGA_VS ENABLED true
set_interface_property VGA_VS EXPORT_OF ""
set_interface_property VGA_VS PORT_NAME_MAP ""
set_interface_property VGA_VS CMSIS_SVD_VARIABLES ""
set_interface_property VGA_VS SVD_ADDRESS_GROUP ""

add_interface_port VGA_VS VGA_VS out Output 1

